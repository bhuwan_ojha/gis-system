<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Property extends Model
{

    protected $table="property_records";
    protected $guarded = ['id'];
}
