<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model
{

    protected $table="news";
    protected $guarded = ['id'];
}
