<?php

namespace App\Libraries;

use App\Models\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PagesController;


class TMHelper
{

    public static function getMenu(){

        $getMenu = new PagesController();

        return $getMenu->getMenu();

    }




    public static function flash($type, $message)
    {
        Session::flash(
            'flash',
            [
                'type'    => $type,
                'message' => $message
            ]
        );
    }

    public static function apiResponse($status, $message, $data = null)
    {
        return [
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ];
    }

    public static function upload(UploadedFile $file, $options = [])
    {

        $options         = [
            'visibility' => isset($options['visibility']) ? $options['visibility'] : 'public'
        ];
        $destinationPath = 'uploads/' . date('Y') . '/' . date('m');

        if ($options['visibility'] == 'public') {
            $destinationPath = 'public/' . $destinationPath;
        }

        Storage::makeDirectory($destinationPath);

        $file->store($destinationPath);

        Artisan::call('storage:link');

        return $file;
    }

    public static function imagePath(File $file = null)
    {

        if ($file == null) {
            return '';
        }

        $path = 'storage/uploads/';
        $path .= date('Y/m', strtotime($file->created_at));
        $path .= '/' . $file->name;

        return asset($path);
    }

    static function getAllStates()
    {

        $stateList = array(
            'प्रदेश नं० १',
            'प्रदेश नं० २',
            'प्रदेश नं० २',
            'गण्डकी प्रदेश',
            'गण्डकी प्रदेश',
            'कर्णाली प्रदेश',
            'सुदूर-पश्चिम प्रदेश'
        );
        return $stateList;
    }

    static function getAllDistrict()
    {
        $districtList = array(
            'धनकुटा',
            'इलाम',
            'झापा',
            'खोटाँग',
            'मोरंग',
            'ओखलढुंगा',
            'पांचथर',
            'संखुवासभा',
            'सोलुखुम्बू',
            'सुनसरी',
            'ताप्लेजुंग',
            'तेह्रथुम',
            'उदयपुर',
            'सप्तरी',
            'सिराहा',
            'धनुषा',
            'महोत्तरी',
            'सर्लाही',
            'बारा',
            'पर्सा',
            'रौतहट',
            'सिन्धुली',
            'रामेछाप',
            'दोलखा',
            'भक्तपुर',
            'धादिङ',
            'काठमाडौँ',
            'काभ्रेपलान्चोक',
            'ललितपुर',
            'नुवाकोट',
            'रसुवा',
            'सिन्धुपाल्चोक',
            'चितवन',
            'मकवानपुर',
            'गोरखा',
            'कास्की',
            'लमजुङ',
            'स्याङग्जा',
            'तनहुँ',
            'मनाङ',
            'नवलपुर',
            'बागलुङ',
            'म्याग्दी',
            'पर्वत',
            'मुस्ताङ',
            'गण्डकी प्रदेश',
            'कपिलवस्तु',
            'परासी',
            'रुपन्देही',
            'अर्घाखाँची',
            'गुल्मी',
            'पाल्पा',
            'दाङ',
            'प्युठान',
            'रोल्पा',
            'पूर्वी रूकुम',
            'बाँके',
            'बर्दिया',
            'पश्चिमी रूकुम',
            'सल्यान',
            'डोल्पा',
            'हुम्ला',
            'जुम्ला',
            'कालिकोट',
            'मुगु',
            'सुर्खेत',
            'दैलेख',
            'जाजरकोट',
            'कैलाली',
            'अछाम',
            'डोटी',
            'बझाङ',
            'बाजुरा',
            'कंचनपुर',
            'डडेलधुरा',
            'बैतडी',
            'दार्चुला'
        );
        return $districtList;
    }


    static function getAllCitizenshipType()
    {
        return $citizenshipType = [
            'विरासत',
        ];
    }

    static function getAllPropertyCategory()
    {

        $categoryList = array(
            'Public Property'=>'सार्वजनिक सम्पत्ति',
            'Private Property'=>'निजी सम्पत्ति',
            'School'=>'स्कूल',
            'Temple'=>'मन्दिर',
            'Cinema Hall'=>'सिनेमा हल',
            'Government Office'=>'सरकारी कार्यालय',
            'Business'=>'व्यवसाय',
            'Hospital'=>'अस्पताल',
            
        );
        return $categoryList;
    }

    static function getAllMaritalStatus()
    {

        $maritalList = array(
            'Single'=>'एकल',
            'Married'=>'विवाहित',
            'Widowed'=>'विधवा',
            'Divorced'=>'तलाक',
        );
        return $maritalList;
    }

    static function getAllNewsCategory()
    {




        $newsCategoryList = array(
            'सामान्य समाचार '=>'General News',
            'सामाजिक समाचार'=> 'Social News',
            'ओटो न्यूज'=>'Auto News',
            'टेक समाचार'=>'Tech News',
            'कानूनी समाचार'=>'Legal News',
            'अपराध समाचार'=>'Crime News',
            'राष्ट्रिय समाचार'=>'National News',
            'अन्तर्राष्ट्रिय समाचार'=>'International News',
            'जीवन शैली समाचार'=>'Lifestyle News',
            'कला समाचार'=>'Art News',
            'मनोरञ्जन समाचार'=>'Entertainment News',
            'खेलकुद समाचार'=>'Sports News',
            'कानूनी समाचार'=>'Legal News',
            'व्यापार समाचार'=>'Business News'
    );
        return $newsCategoryList;
    }

    static function getAllBloodGroups()
    {
        $bloodGroup = array(
            'O+',
            'O-',
            'B+',
            'B-',
            'AB+',
            'AB-',
            'A+',
            'A-'
        );
        return $bloodGroup;
    }

    static  function CheckAuth(){

        if(session()->get('userRoles')->user_roles == 'manager') {

            echo 'You Have No Permission To Be Here';
            die();
        }
    }


}
