<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','location_id','user_role_id','ward_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static function getUserNameById($userId)
    {
        return User::find($userId)->first();
    }

    public function sendPasswordResetNotification($token)
    {

        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

}
