<?php

namespace App\Http\Controllers;

use App\Services\PagesService;
use App\Services\NoticeService;
use Illuminate\Support\Facades\Auth;
use App\Pages;
use View;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->pages = new PagesService(new Pages());
       // $this->pages = $pagesService;

    }


    function  getMenu(){

        return $this->pages->getMenu();
    }
}
