<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Record;
use App\Services\PropertyService;
use App\Services\RecordService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class PropertyController extends Controller
{
    public function __construct(PropertyService $propertyService , RecordService $recordService)
    {
        $this->middleware('auth');
        $this->property = $propertyService;
        $this->record = $recordService;
    }

    public function index()
    {
        if (session()->get('userRoles')->user_roles == 'superadmin') {
            $property = $this->property->getAllProperty(10);
        }elseif (session()->get('userRoles')->user_roles == 'admin') {
            $property = $this->property->getMyProperty(Auth::user()->property_id,10);
        }
        return view('admin.property-record',compact('property'));
    }

    public function propertyForm($propertyId=null)
    {

        $property = null;
        $citizenshipNumber = $this->record->getAllRecord();
        if ($propertyId){
            $property = $this->property->getByPropertyId($propertyId);
            return view('admin.property-record-form',compact('property','citizenshipNumber'));
        }else{
            return view('admin.property-record-form',compact('property','citizenshipNumber'));
        }
    }


    public function saveUpdateProperty(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'citizenship_number' => 'required',
            'property_location_state' => 'required',
            'property_location_district' => 'required',
            'property_location_municipality' => 'required',
            'property_location_ward_no' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $propertyData = $request->except('_token','coordinates');
        if ($request->file('property_map_image')){
        $citizenshipImage = isset($propertyData['property_map_image']) ? $propertyData['property_map_image'] : '';
        if (isset($citizenshipImage) && $citizenshipImage !== null) {
            $image = $citizenshipImage;
            $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/property');
            $image->move($destinationPath, $input['imagename']);
            $citizenshipImage = $input['imagename'];
            $propertyData['property_map_image'] = $citizenshipImage;
        } else {
            unset($citizenshipImage);
        }

    }
        if ($request->file('property_image')){
            $propertyImage = isset($propertyData['property_image']) ? $propertyData['property_image'] : '';
            if (isset($propertyImage) && $propertyImage !== null) {
                $image = $propertyImage;
                $input['propertyimage'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/property');
                $image->move($destinationPath, $input['propertyimage']);
                $propertyImage = $input['propertyimage'];
                $propertyData['property_image'] = $propertyImage;
            } else {
                unset($propertyImage);
            }

        }
        $coordinate = $request->input('coordinates');
        $propertyData['unique_id'] = $this->generateRefId();
        $propertyData['added_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->property->saveUpdate($propertyData,$coordinate);
            TMHelper::flash('success', 'Property Record added successfully.');
            return redirect('/admin/property');
        }else{
            $this->property->updateProperty($propertyData,$coordinate);
            TMHelper::flash('success', 'Property Record updated successfully.');
            return redirect('/admin/property');
        }

    }

    public function deleteProperty($id)
    {
        $this->property->delete($id);
        TMHelper::flash('success', 'Property Record deleted successfully.');
        return redirect()->back();
    }


    public function generateRefId()
    {
        $refId = '#PROP' . rand(11111, 99999);

        if (Record::where('unique_id', $refId)->first()) {
            return $this->generateRefId();
        }

        return $refId;
    }

}
