<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Services\NoticeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class NoticeController extends Controller
{
    public function __construct(NoticeService $noticeService,LocationService $locationService)
    {
        $this->middleware('auth');
        $this->notice = $noticeService;
        $this->location = $locationService;
    }

    public function index()
    {

      $noticeList = $this->notice->getAllNotice(10,session()->get('userRoles')->user_roles);

        return view('admin.notice',compact('noticeList'));
    }

    public function noticeForm($noticeId=null)
    {

        $notice = null;
        $wards = $this->location->getWardByLocationId();
        if ($noticeId){
            $notice = $this->notice->getByNoticeId($noticeId);
            return view('admin.notice-form',compact('notice','wards'));
        }else{
            return view('admin.notice-form',compact('notice','wards'));
        }
    }


    public function saveUpdateNotice(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'notice_title' => 'required',
            'notice_description' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $noticeData = $request->except('_token');
        if ($request->file('notice_media')) {
            $noticeMedia = isset($noticeData['notice_media']) ? $noticeData['notice_media'] : '';
            if (isset($noticeMedia) && $noticeMedia !== null) {
                $image = $noticeMedia;
                $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/notice/notice_files');
                $image->move($destinationPath, $input['imagename']);
                $noticeMedia = $input['imagename'];
                $noticeData['notice_media'] = $noticeMedia;
            } else {
                unset($noticeMedia);
            }
        }

        $noticeData['created_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->notice->saveUpdate($noticeData);
            TMHelper::flash('success', 'Notice added successfully.');
            return redirect('/admin/notice');
        }else{
            $this->notice->updateNotice($noticeData);
            TMHelper::flash('success', 'Notice updated successfully.');
            return redirect('/admin/notice');
        }

    }

    public function deleteNotice($id)
    {
        $this->notice->delete($id);
        TMHelper::flash('success', 'Notice deleted successfully.');
        return redirect()->back();
    }




}
