<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Record;
use App\Services\PublicPropertyService;
use App\Services\RecordService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class PublicPropertyController extends Controller
{
    public function __construct(PublicPropertyService $propertyService , RecordService $recordService)
    {
        $this->middleware('auth');
        $this->property = $propertyService;
        $this->record = $recordService;
    }

    public function index()
    {

            $property = $this->property->getAllPublicProperty(10,session()->get('userRoles')->user_roles);

        return view('admin.public-property-record',compact('property'));
    }

    public function propertyForm($propertyId=null)
    {

        $property = null;
        $citizenshipNumber = $this->record->getAllRecord();
        if ($propertyId){
            $property = $this->property->getByPropertyId($propertyId);
            return view('admin.public-property-record-form',compact('property','citizenshipNumber'));
        }else{
            return view('admin.public-property-record-form',compact('property','citizenshipNumber'));
        }
    }


    public function saveUpdateProperty(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'kitta_number' => 'required',
            'mode_number' => 'required',
            'area' => 'required',
            'property_title' => 'required',
            'property_location_state' => 'required',
            'property_location_district' => 'required',
            'property_location_municipality' => 'required',
            'property_location_ward_no' => 'required',
            'property_category' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $propertyData = $request->except('_token','coordinates');
        if ($request->file('property_map_image')){
        $property_map_image = isset($propertyData['property_map_image']) ? $propertyData['property_map_image'] : '';
        if (isset($property_map_image) && $property_map_image !== null) {
            $image = $property_map_image;
            $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/property');
            $image->move($destinationPath, $input['imagename']);
            $property_map_image = $input['imagename'];
            $propertyData['property_map_image'] = $property_map_image;
        } else {
            unset($property_map_image);
        }

    }
        if ($request->file('property_image')){
            $propertyImage = isset($propertyData['property_image']) ? $propertyData['property_image'] : '';
            if (isset($propertyImage) && $propertyImage !== null) {
                $image = $propertyImage;
                $input['propertyimage'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/property');
                $image->move($destinationPath, $input['propertyimage']);
                $propertyImage = $input['propertyimage'];
                $propertyData['property_image'] = $propertyImage;
            } else {
                unset($propertyImage);
            }

        }
        $coordinate = $request->input('coordinates');
        $propertyData['unique_id'] = $this->generateRefId();
        $propertyData['added_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->property->saveUpdate($propertyData,$coordinate);
            TMHelper::flash('success', 'Property Record added successfully.');
            return redirect('/admin/public-property');
        }else{
            $this->property->updateProperty($propertyData,$coordinate);
            TMHelper::flash('success', 'Property Record updated successfully.');
            return redirect('/admin/public-property');
        }

    }

    public function deleteProperty($id)
    {
        $this->property->delete($id);
        TMHelper::flash('success', 'Property Record deleted successfully.');
        return redirect()->back();
    }


    public function generateRefId()
    {
        $refId = '#PROP' . rand(11111, 99999);

        if (Record::where('unique_id', $refId)->first()) {
            return $this->generateRefId();
        }

        return $refId;
    }

}
