<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Record;
use App\Services\RecordService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class RecordController extends Controller
{
    public function __construct(RecordService $recordService)
    {
        $this->middleware('auth');
        $this->record = $recordService;
    }

    public function index()
    {

      $records = $this->record->getAllRecord(10,session()->get('userRoles')->user_roles);

        return view('admin.personal-record',compact('records'));
    }

    public function recordForm($recordId=null)
    {

        $records = null;
        $headofFamily = $this->record->getAllHeadOfFamily();
        if ($recordId){
            $records = $this->record->getByRecordId($recordId);
            //dd($records);
            return view('admin.personal-record-form',compact('records','headofFamily'));
        }else{
            return view('admin.personal-record-form',compact('records','headofFamily'));
        }
    }


    public function saveUpdateRecord(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'birth_place'=>'required',
            'age' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'marital_status' => 'required',
            'permanent_address_state' => 'required',
            'permanent_address_district' => 'required',
            'permanent_address_municipality' => 'required',
            'permanent_address_ward_no' => 'required',
            'citizenship_type' => 'required',
            'citizenship_number' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $recordData = $request->except('_token');
        if ($request->file('citizenship_image_front')) {
            $citizenshipImage = isset($recordData['citizenship_image_front']) ? $recordData['citizenship_image_front'] : '';
            if (isset($citizenshipImage) && $citizenshipImage !== null) {
                $image = $citizenshipImage;
                $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/citizenship/' . $recordData['citizenship_number']);
                $image->move($destinationPath, $input['imagename']);
                $citizenshipImage = $input['imagename'];
                $recordData['citizenship_image_front'] = $citizenshipImage;
            } else {
                unset($citizenshipImage);
            }
        }

        if ($request->file('citizenship_image_back')) {
            $citizenshipBackImage = isset($recordData['citizenship_image_back']) ? $recordData['citizenship_image_back'] : '';
            if (isset($citizenshipBackImage) && $citizenshipBackImage !== null) {
                $backImage = $citizenshipBackImage;
                $input['backimage'] = time() . '_back' . '.' . $backImage->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/citizenship/' . $recordData['citizenship_number']);
                $backImage->move($destinationPath, $input['backimage']);
                $citizenshipBackImage = $input['backimage'];
                $recordData["citizenship_image_back"] = $citizenshipBackImage;
            } else {
                unset($citizenshipBackImage);
            }
        }

        if(isset($recordData['pariwar_no']) && $recordData['pariwar_no'] !='') { $recordData['head_of_family'] = 0; }

        $recordData['unique_id'] = $this->generateRefId();
        $recordData['full_name'] = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
        unset($recordData['first_name'],$recordData['middle_name'],$recordData['last_name']);
        $recordData['added_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->record->saveUpdate($recordData);
            TMHelper::flash('success', 'Record added successfully.');
            return redirect('/admin/records');
        }else{
            $this->record->updateRecord($recordData);
            TMHelper::flash('success', 'Record updated successfully.');
            return redirect('/admin/records');
        }

    }

    public function deleteRecord($id)
    {
        $this->record->delete($id);
        TMHelper::flash('success', 'Record deleted successfully.');
        return redirect()->back();
    }


    public function generateRefId()
    {
        $refId = '#REC' . rand(11111, 99999);

        if (Record::where('unique_id', $refId)->first()) {
            return $this->generateRefId();
        }

        return $refId;
    }

    public function getRecordByCitizenshipNumber(Request $request)
    {
        $citizenshipNumber = $request->input('q');
        if ($citizenshipNumber) {
            return json_encode($this->record->getRecordByCitizenshipNumber($citizenshipNumber));
        }
    }

}
