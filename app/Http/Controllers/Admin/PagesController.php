<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Services\PagesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class PagesController extends Controller
{
    public function __construct(PagesService $pagesService,LocationService $locationService)
    {
        $this->middleware('auth');
        $this->pages = $pagesService;
        $this->location = $locationService;
    }

    public function index()
    {


      $pagesList = $this->pages->getAllpages(10,session()->get('userRoles')->user_roles);

       // var_dump($pagesList);

       return view('admin.pages',compact('pagesList'));
    }

    public function pagesForm($pagesId=null)
    {

        $pages = null;
        $slugs = $this->pages->getAllSlug();


        if ($pagesId){
            $pages = $this->pages->getBypagesId($pagesId);
            $slugs = $this->pages->getAllSlug($pagesId);
        }

        return view('admin.pages-form',compact('pages','slugs'));

    }


    public function saveUpdatepages(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'page_title' => 'required',
            'page_description' => 'required',
            'page_slug' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $pagesData = $request->except('_token','files');
        if ($request->file('page_media')) {
            $pagesMedia = isset($pagesData['page_media']) ? $pagesData['page_media'] : '';
            if (isset($pagesMedia) && $pagesMedia !== null) {
                $image = $pagesMedia;
                $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/pages/pages_files');
                $image->move($destinationPath, $input['imagename']);
                $pagesMedia = $input['imagename'];
                $pagesData['page_media'] = $pagesMedia;
            } else {
                unset($pagesMedia);
            }
        }

        $pagesData['created_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->pages->saveUpdate($pagesData);
            TMHelper::flash('success', 'Pages Added Successfully.');
            return redirect('/admin/Pages');
        }else{
            $this->pages->updatepages($pagesData);
            TMHelper::flash('success', 'Page Updated Successfully.');
            return redirect('/admin/Pages');
        }

    }

    public function deletepages($id)
    {
        $this->pages->delete($id);
        TMHelper::flash('success', 'Page deleted successfully.');
        return redirect()->back();
    }




}
