<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Services\NewsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class NewsController extends Controller
{
    public function __construct(NewsService $newsService,LocationService $locationService)
    {
        $this->middleware('auth');

        $this->news = $newsService;
        $this->location = $locationService;
    }

    public function index()
    {


        $newsList = $this->news->getAllNews(10,session()->get('userRoles')->user_roles);
        return view('admin.news',compact('newsList'));
    }

    public function newsForm($newsId=null)
    {

        $news = null;
        $wards = $this->location->getWardByLocationId();
        if ($newsId){
            //var_dump($newsId);
            $news = $this->news->getByNewsId($newsId);
            //dd($news);
            return view('admin.news-form',compact('news','wards'));
        }else{
            return view('admin.news-form',compact('news','wards'));
        }
    }


    public function saveUpdateNews(Request $request)
    {


        $validation = Validator::make($request->all(), [
            'news_title' => 'required',
            'news_description' => 'required',
            'news_category' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }

        $newsData = $request->except('_token');
        if ($request->file('files')) {
            $newsMedia = isset($newsData['files']) ? $newsData['files'] : '';
            if (isset($newsMedia) && $newsMedia !== null) {
                $image = $newsMedia;
                $input['imagename'] = time() . '_front' . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/news');
                $image->move($destinationPath, $input['imagename']);
                $newsMedia = $input['imagename'];
                $newsData['files'] = $newsMedia;
            } else {
                unset($newsMedia);
            }
        }

        $newsData['created_by'] = session()->get('users')->id;
        if ($request->id ==null){
            $this->news->saveUpdate($newsData);
            TMHelper::flash('success', 'News added successfully.');
            return redirect('/admin/news');
        }else{
            $this->news->updateNews($newsData);
            TMHelper::flash('success', 'News updated successfully.');
            return redirect('/admin/news');
        }

    }

    public function deleteNews($id)
    {
        $this->news->delete($id);
        TMHelper::flash('success', 'News deleted successfully.');
        return redirect()->back();
    }




}
