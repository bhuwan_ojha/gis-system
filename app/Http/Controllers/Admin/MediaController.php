<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\BookingService;
use App\Services\MailService;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use View;

class MediaController extends Controller
{
    public function __construct(MediaService $mediaService)
    {
        $this->middleware('auth');
        $this->media = $mediaService;
    }

    public function index()
    {

        $images = $this->media->getAllImages(10);
        $videos = $this->media->getAllVideos(10);
        return view('admin.media',compact('images','videos'));
    }

    public function mediaForm($id=null)
    {
        if ($id !== null) {
            $media = $this->media->getByMediaId($id);
            return view('admin.media-form', compact('media'));
        }else{
            return view('admin.media-form');
        }
    }


    public function saveUpdateMedia(Request $request)
    {
        $data = $request->except('_token');
        $media=array();


        if ($request->file('media_image') !==null){
            foreach ($request->file('media_image') as $data_key => $data_value) {
                if ($request->file('media_image')[$data_key] !== null) {
                    $file = $request->file('media_image')[$data_key];
                    $destinationPath = public_path() . '/uploads/media';
                    $filename = time() . '_' . $file->getClientOriginalName();
                    $filename = str_replace(' ', '_', $filename);
                    $fileName = $file->move($destinationPath, $filename);
                    $media[] = array(
                        'media_image' => $filename,
                        'ID' => $request->ID[$data_key]
                    );
                }
            }
        }else{
            $media[] = array(
                "video_url" => TMHelper::getYoutubeEmbedUrl($request->video_url),
                "video_title" =>$request->video_title,
                "video_description" =>$request->video_description,
                "ID" => $request->ID
            );
        }

        if (isset($media[0]->ID) && $request[0]->ID !== null) {
            $this->media->saveUpdate($media);
            TMHelper::flash('success','Media Details Updated Successfully.');
            return redirect('/admin/media');
        } else {
            $this->media->saveUpdate($media);
            TMHelper::flash('success','New Media Added Successfully.');
            return redirect('/admin/media');
        }
    }


    public function deleteMedia($id)
    {
        $this->media->delete($id);
        return redirect()->back();
    }

}
