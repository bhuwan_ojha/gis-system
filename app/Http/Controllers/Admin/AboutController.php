<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\BookingService;
use App\Services\MailService;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use View;
use App\Services\AboutService;

class AboutController extends Controller
{
    public function __construct(AboutService $aboutservice)
    {
        $this->middleware('auth');
        $this->about = $aboutservice;
    }

    public function index()
    {

        $images = $this->about->getAllImages(10);
        $videos = $this->about->getAllVideos(10);
        return view('admin.about',compact('images','videos'));
    }

    public function mediaForm($id=null)
    {
        if ($id !== null) {
            $media = $this->about->getByMediaId($id);
            return view('admin.about-form', compact('media'));
        }else{
            return view('admin.about-form');
        }
    }


    public function saveUpdateMedia(Request $request)
    {
        $data = $request->except('_token');
        $media=array();
        if ($request->file('media_image') !==null) {
            foreach ($request->file('media_image') as $data_key => $data_value) {
                if ($request->file('media_image')[$data_key] !== null) {
                    $file = $request->file('media_image')[$data_key];
                    $destinationPath = public_path() . '/uploads/about';
                    $filename = time() . '_' . $file->getClientOriginalName();
                    $filename = str_replace(' ', '_', $filename);
                    $fileName = $file->move($destinationPath, $filename);
                    $media[] = array(
                        'media_image' => $filename,
                        'ID' => $request->ID[$data_key]
                    );
                }
            }
        }

        if (isset($media[0]->ID) && $request[0]->ID !== null) {
            $this->about->saveUpdate($media);
            TMHelper::flash('success','Media Details Updated Successfully.');
            return redirect('/admin/about');
        } else {
            $this->about->saveUpdate($media);
            TMHelper::flash('success','New Media Added Successfully.');
            return redirect('/admin/about');
        }
    }


    public function deleteMedia($id)
    {
        $this->about->delete($id);
        return redirect()->back();
    }

}
