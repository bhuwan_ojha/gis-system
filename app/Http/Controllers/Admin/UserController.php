<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Services\MailService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use View;

class UserController extends Controller
{
    public function __construct(UserService $userService, MailService $mailService , LocationService $locationService)
    {

        $this->middleware('auth');
       // dd(Auth::check()->user());
      //  dd(session()->get('userRoles'));

        $this->user = $userService;
        $this->mail = $mailService;
        $this->location = $locationService;


    }

    public function index()
    {
        TMHelper::CheckAuth();
        $locationId = session()->get('users')->location_id;

        $users = $this->user->getAllUsers(10,session()->get('userRoles')->user_roles);

        return view('admin.users',compact('users'));
    }

    public function userForm($userId=null)
    {

        TMHelper::CheckAuth();
        $user = $this->user->getByUserId($userId);
        $locations = $this->location->getAllLocation();
        $wards = $this->location->getWardByLocationId(session()->get('users')->location_id);
        $userRoles = $this->user->getUserRoles();
        if ($userId){
            return view('admin.user-form',compact('user','userRoles','locations','wards'));
        }else{
            return view('admin.user-form',compact('user','userRoles','locations','wards'));
        }
    }

    public function profile()
    {
        $userId = Auth::user()->id;
        if ($userId !== null) {
            $user = $this->user->getByUserId($userId);
            return view('admin.profile', compact('user'));
        }
    }


    public function saveUpdateUser(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'password' => 'required',
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'location_id' => 'required',
            'user_role_id' => 'required',
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }
        $userData = $request->except('_token');
        $userData['password'] = bcrypt($userData['password']);

        if ($request->id ==null){
            $this->user->saveUpdate($userData);
            TMHelper::flash('success', 'User added successfully.');
            return redirect('/admin/users');
        }else{
            $this->user->updateUser($userData);
            TMHelper::flash('success', 'User details updated successfully.');
            return redirect('/admin/users');
        }

    }

    public function updateProfile(Request $request)
    {
        $userData = $request->except('_token');
        if ($userData['new_password']!==null)
        {
            $userData['password'] = bcrypt($userData['new_password']);
        }
        unset($userData['new_password']);
            $this->user->updateUser($userData);
            TMHelper::flash('success', 'User details updated successfully.');
            return redirect('admin/users/profile');
        }



    public function deleteUser($id)
    {
        $this->user->delete($id);
        return redirect()->back();
    }

    public function activateUser($id)
    {
        $this->user->activate($id);
        return redirect()->back();
    }


    public function deactivateUser($id)
    {
        $this->user->deactivate($id);
        return redirect()->back();
    }


}
