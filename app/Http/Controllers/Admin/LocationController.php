<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Ward;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use View;

class LocationController extends Controller
{
    public function __construct(LocationService $locationService,Ward $ward)
    {

        $this->location = $locationService;
        $this->ward = $ward;
    }

    public function index()
    {
        if (session()->get('userRoles')->user_roles == 'superadmin') {
            $locations = $this->location->getAllLocation(10);
        }elseif (session()->get('userRoles')->user_roles == 'admin') {
            $locations = $this->location->getAllLocation(10);
        }
        return view('admin.location',compact('locations','wards'));
    }

    public function locationForm($locationId=null)
    {

        $locations = null;
        if ($locationId){
            $locations = $this->location->getByLocationId($locationId);
            $wards = $this->location->getWardByLocationId($locationId);
            return view('admin.location-form',compact('locations','wards'));
        }else{
            return view('admin.location-form',compact('locations'));
        }
    }


    public function saveUpdateLocation(Request $request)
    {

      //  dd($_POST);
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'state' => 'required',
            'district' => 'required',
            'ward_no' => 'required',
            'ward_address' => 'required',

        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', '*All Fields Required');
            return redirect()->back()->withErrors($validation->errors());
        }
        $locationData = $request->except('_token', 'ward_no', 'ward_address', 'ward_image','ward_id');
        //dd($_POST);
        //dd($request->ward_image);

        $wardData = array(
            'ward_no' => $request->ward_no,
            'ward_address' => $request->ward_address,
            'ward_image' => $request->ward_image,
            'ward_id' => $request->ward_id,
        );
        $wardInfo = array();
        $wardImage = isset($wardData['ward_image']) ? $wardData['ward_image'] : '';
/*
        var_dump($wardImage);
        dd($wardData);*/
        foreach ($wardData['ward_no'] as $data_key=>$data_value) {
            //dd($wardImage[$data_key]);
           // var_dump($i);
            /*if ($request->file('ward_image') && !empty($request->file('ward_image'))) {*/
                if(isset($wardImage[$data_key])) {
                    $image = $wardImage[$data_key];
                    $input['imagename'][$data_key] = time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/ward/');
                    $image->move($destinationPath, $input['imagename'][$data_key]);
                    $wardImage[$data_key] = $input['imagename'][$data_key];
                    $wardInfo[] = array(
                        "ward_no" => $wardData['ward_no'][$data_key],
                        "ward_address" => $wardData['ward_address'][$data_key],
                        "ward_image" => $wardImage[$data_key],
                        "created_by" => Auth::user()->id,
                        "location_id" => Auth::user()->location_id,
                        "id" => isset($wardData['ward_id'][$data_key]) ? $wardData['ward_id'][$data_key] : ''
                    );

            } else {

                $wardInfo[] = array(
                    "ward_no" => $wardData['ward_no'][$data_key],
                    "ward_address" => $wardData['ward_address'][$data_key],
                    "location_id" => Auth::user()->location_id,
                    "created_by" => Auth::user()->id,
                    "id" => isset($wardData['ward_id'][$data_key]) ? $wardData['ward_id'][$data_key] : ''
                );
                unset($wardData['ward_image'][$data_key]);
            }
        }
            if ($request->id == null) {
                $this->location->saveUpdate($locationData,$wardInfo);
                TMHelper::flash('success', 'Location added successfully.');
                return redirect('/admin/locations');
            } else {
                $this->location->updateLocation($locationData,$wardInfo);
                TMHelper::flash('success', 'Location updated successfully.');
                return redirect('/admin/locations');

        }
    }

    public function deleteLocation($id)
    {
        $this->location->delete($id);
        $this->ward->where('location_id',$id)->delete($id);
        TMHelper::flash('success', 'Location deleted successfully.');
        return redirect()->back();
    }



    public function deleteWardData()
    {
        $wardID = $_POST['wardID'];

        $this->location->deleteWardByID($wardID);

    }

}
