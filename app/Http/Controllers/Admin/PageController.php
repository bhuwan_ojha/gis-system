<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\RecordService;
use View;

class PageController extends Controller
{
    public function __construct(TMHelper $TMHelper,RecordService $recordService)
    {
        $this->middleware('auth');
        $this->tmhelper = $TMHelper;
        $this->record = $recordService;
    }

    public function getPage($slug = null)
    {
        switch ($slug) {
            case 'dashboard':
                $category = $this->tmhelper->getAllPropertyCategory();


                $categoryArray = array();
                foreach ($category as $key=>$data) {
                    $categoryArray[$key] = array();
                }

                $categorywithdata = $this->record->getAllCategoryData();

                foreach ($categorywithdata as $value) {

                    if($value['property_category'] != '' && is_array($categoryArray[$value['property_category']])){
                        array_push($categoryArray[$value['property_category']], $value);

                    }
                }

                $allCategoryLocationJsonData =  json_encode($categoryArray);

                return view('admin.index',compact('category','allCategoryLocationJsonData'));
                break;

            default:
                if (View::exists('admin.'.$slug)) {
                    return view('admin.'.$slug);
                }
        }

        return view('admin.404');
    }
}
