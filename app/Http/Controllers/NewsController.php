<?php

namespace App\Http\Controllers;

use App\Services\LocationService;
use App\Services\NewsService;
use Illuminate\Support\Facades\Auth;
use View;

class NewsController extends Controller
{
    public function __construct(NewsService $newsService,LocationService $locationService)
    {
        $this->news = $newsService;
        $this->location = $locationService;
    }

        public function index($wardId=null)
        {

        if ($wardId){
            $newsList =   $this->news->getAllNewsByWardId($wardId,$page=1);

            }else{
            $newsList = $this->news->getAllNews($page=1);
        }
        return view('frontend.news-list',compact('newsList'));
        }

        function getNewsDetail($wardId,$newsId)
        {
            $news =   $this->news->getWardNewsById($wardId,$newsId);
            $moreNews =   $this->news->getMoreNews($wardId,$newsId,$page=4);
            return view('frontend.news-detail',compact('news','moreNews'));

        }
}
