<?php

namespace App\Http\Controllers;

use App\Libraries\TMHelper;
use App\Services\LocationService;
use App\Services\MediaService;
use App\Services\NewsService;
use App\Services\NoticeService;
use App\Services\RecordService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Services\PagesService;


class PageController extends Controller
{
    public function __construct(RecordService $recordService,
                                LocationService $locationService,
                                NewsService $newsService,
                                NoticeService $noticeService,
                                TMHelper $TMHelper,
                                MediaService $mediaService,
                                PagesService $pagesService

    )
    {
        $this->record = $recordService;
        $this->location = $locationService;
        $this->news = $newsService;
        $this->notice = $noticeService;
        $this->tmhelper = $TMHelper;
        $this->media = $mediaService;
        $this->pages = $pagesService;
        $this->request = new Request();

    }

    public function getPage($slug = null)
    {


        switch ($slug) {
            case '':
                $countTotalPopulation = $this->record->countTotalPopulation();
                $countTotalOld = $this->record->countTotalOld();
                $countTotalWidow = $this->record->countTotalWidow();
                $countTotalPhysicalDisabled = $this->record->countTotalPhysicalDisabled();
                $countTotalWards = $this->location->countTotalWards();
                $newsList = $this->news->getAllNews(5);
                $noticeList = $this->notice->getAllNotice(5);
                $category = $this->tmhelper->getAllPropertyCategory();

                //unset($category['Private Property']);

                $allWardList = $this->location->getAllWards();
                $aboutUs = $this->location->getAboutUS();




                return view('frontend.index',compact(
                    'countTotalPopulation',
                    'countTotalOld',
                    'countTotalWidow',
                    'countTotalPhysicalDisabled',
                    'countTotalWards',
                    'newsList',
                    'noticeList',
                    'category',
                    'allWardList',
                    'aboutUs'
                ));
                break;
            case 'gallery':
                $images = $this->media->getAllImages(10);
                $videos = $this->media->getAllVideos(10);
                return view('frontend.gallery',compact('images','videos'));
                break;

            case 'map':
                $category = $this->tmhelper->getAllPropertyCategory();

                $categoryArray = array();
                foreach ($category as $key=>$data) {
                    $categoryArray[$key] = array();
                }

                $categorywithdata = $this->record->getAllCategoryData();

                foreach ($categorywithdata as $value) {

                    if($value['property_category'] != '' && is_array($categoryArray[$value['property_category']])){
                        array_push($categoryArray[$value['property_category']], $value);

                    }
                }

                $allCategoryLocationJsonData =  json_encode($categoryArray);

                return view('frontend.map',compact(
                    'category',
                    'allCategoryLocationJsonData'

                ));
                break;


                case 'contact':

                 $messageType =  session('messagetype');
                 $message = session('message');

                 $altermessage = '';
                 if($messageType =='success') {

                     $altermessage = '<p style=" padding:8px; border-radius:4px;margin-top:4px;background: green;color: white">'.$message.'</p>';
                 } elseif ($messageType == 'fail') {

                     $altermessage = '<p style="padding:8px; border-radius:4px;margin-top:4px;background: red;color: white">'.$message.'</p>';
                 }
                 return view('frontend.contact',compact(
                     'altermessage'

                 ));
                break;



            default:

             $pagesData =  $this->pages->getBySlug($slug);

             if(count($pagesData)){

             return view('frontend.pages-detail',compact('pagesData'));

             }



                if (View::exists('frontend.'.$slug)) {
                    return view('frontend.'.$slug);
                }
        }

        return view('frontend.404');
    }

    function getMapSearchResult(){

        $name = $_POST['name'];
        $categoryList = $_POST['categorylist'];


        if($name =='') return json_encode(array());

        return $this->record->getLocationData($name,$categoryList);

    }
    function sendMail(){



                session(['messagetype' => '']);
                session(['message' => '']);

        $email = $_POST['email'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $message = $_POST['message'];

                include app_path().'\Libraries\PHPMailer\PHPMailerAutoload.php';

                $mail = new \PHPMailer();
                $mail->CharSet = "UTF-8";

                $mail->isSMTP();
                $mail->Debugoutput = 'html';

                $mail->Host = 'smtp.gmail.com';

                $mail->Port = 465;
                $mail->SMTPSecure = 'ssl';
                $mail->SMTPAuth = true;
                $mail->Username = "gissystem2019@gmail.com";
                $mail->Password = "Gis@12345";
                $mail->From = $email;
                $mail->FromName = $first_name.' '.$last_name.' ('.$email.')';

                $mail->addAddress($email);


                $mail->Subject = 'Contact FeedBack Message';
                $mail->msgHTML($message);
                if($mail->send()){
                    session(['messagetype' => 'success']);
                    session(['message' => 'Feedback SuccessFully Sent']);


                } else {

                    session(['messagetype' => 'fail']);
                    session(['message' => 'Failed Sending Feedback']);

                }


        return redirect('/contact');

    }




}
