<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\TMHelper;
use App\Services\LoginService;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class LoginController
{


    private $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function Authenticate(Request $request)
    {
        $data = $request->all();
        $user = $this->loginService->authenticateUser($data);
        if ($user) {
            return redirect('/');
        } else {
            TMHelper::flash('error', 'Invalid Credentials');
            return redirect()->back();
        }

    }

    public function showLoginForm()
    {
        TMHelper::flash('error','You must log in first');
        return redirect('/admin')->with('hello');
    }

    public function adminLogin(Request $request)
    {
        $data = $request->all();
        $user = $this->loginService->checkAdmin($data);
        if ($user) {
            return redirect('/admin/dashboard');
        } else {
            TMHelper::flash('error', 'Invalid Credentials');
            return redirect()->back();
        }

    }
    public function postRegister(Request $request)
    {
        $data = $request->all();
        $this->loginService->registerUser($data);
        TMHelper::flash('success', 'Account Register Successfully.Please login here.');
        return redirect('/admin');


    }

    public function logout()
    {
        $this->loginService->logout();
        return redirect()->back();
        //return redirect('/admin');
    }

    public function adminLogout()
    {
        $this->loginService->logout();
        return redirect('/admin');
    }


    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), [
            'password' => 'hash:' . $user->password,
            'new_password' => 'required'
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', 'Invalid password.');
            return redirect()->back()->withErrors($validation->errors());
        }

        if (Hash::check($request->current_password, $user->password)) {
            if ($request->new_password == $request->confirm_password) {
                $user->password = Hash::make($request->input('new_password'));
                $user->save();

                TMHelper::flash('success', 'Your new password is now set!');
                return redirect()->back();
            } else {
                TMHelper::flash('errors', 'Your confirmation password do not match!');
                return redirect()->back();
            }
        }else{
            TMHelper::flash('error', 'Your old password do not match!');
            return redirect()->back();
        }

    }

    public function deleteAccount(Request $request)
    {
        $userId = $request->user_id;
        $this->loginService->deleteAccount($userId);
        return redirect()->back();
    }


}
