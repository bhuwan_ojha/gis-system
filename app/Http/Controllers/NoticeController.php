<?php

namespace App\Http\Controllers;

use App\Services\LocationService;
use App\Services\NoticeService;
use Illuminate\Support\Facades\Auth;
use View;

class NoticeController extends Controller
{
    public function __construct(NoticeService $noticeService,LocationService $locationService)
    {
        $this->notice = $noticeService;
        $this->location = $locationService;
    }

        public function index($wardId=null,$noticeId=null)
        {

        if ($wardId){
            $noticeList =   $this->notice->getAllNoticeByWardId($wardId,$page=1);
            }else{
            $noticeList = $this->notice->getAllNotice($page=1);
        }
        return view('frontend.notice-list',compact('noticeList'));
        }

        function getNoticeDetail($wardId,$noticeId)
        {
            $moreNotice =   $this->notice->getMoreNotice($wardId,$noticeId,$page=4);
            $notice =   $this->notice->getWardNoticeById($wardId,$noticeId);
            return view('frontend.notice-detail',compact('notice','moreNotice'));

        }

    function  getMenu(){

        return $this->notice->getMenu();
    }
}
