<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pages extends Model
{

    protected $table="pages";
    protected $guarded = ['id'];
}
