<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRoles extends Model
{

    protected $fillable = ['user_id','user_roles'];

    public function getUserRoles($id)
    {
        $userRoles = $this->where('id',$id)->get()->first();
        return $userRoles;

    }
}
