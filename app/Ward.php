<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ward extends Model
{

    protected $table="wards";
    protected $guarded = ['id'];
}
