<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Record extends Model
{

    protected $table="personal_records";
    protected $guarded = ['id'];
}
