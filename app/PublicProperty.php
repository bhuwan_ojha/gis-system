<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PublicProperty extends Model
{

    protected $table="public_property_records";
    protected $guarded = ['id'];
}
