<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notice extends Model
{

    protected $table="notice";
    protected $guarded = ['id'];
}
