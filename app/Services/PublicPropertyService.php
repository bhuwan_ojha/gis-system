<?php

namespace App\Services;


use App\PublicProperty;
use Illuminate\Support\Facades\DB;

class PublicPropertyService
{
    public function __construct(PublicProperty $property)
    {
        $this->property = $property;
    }

    public function saveUpdate($propertyData)
    {
        DB::beginTransaction();
        try {
           $savePropertyData =  $this->property->create($propertyData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }



    public function updateProperty($propertyData)
    {
        try {

                $updateProperty = $this->property->where(['id' => $propertyData['id']])->update($propertyData);
                return $updateProperty;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByPropertyId($id)
    {
        $user = $this->property->find($id);
        return $user;
    }

    public function delete($id)
    {
        $deleteUser = $this->property->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllPublicProperty($page=null,$role = NULL)
    {

        if($role == NULL) {
            $userList = $this->property->orderBy('id','ASC')->paginate($page);
        }
        else if ($role == 'superadmin') {

            $userList = $this->property->orderBy('id', 'ASC')->paginate($page);

        }
        else if ($role == 'admin') {

            $userList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'public_property_records.property_location_ward_no')->where('property_location_ward_no', session()->get('users')->ward_no)->paginate($page);
        }
        else if ($role == 'manager') {

            $userList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'public_property_records.property_location_ward_no')->where('public_property_records.created_by', session()->get('users')->id)->paginate($page);
        }

        return $userList;
    }

}