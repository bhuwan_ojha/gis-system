<?php
/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2018-03-03
 * Time: 5:48 PM
 */

namespace App\Services;
use App\Booking;
use App\Libraries\TMHelper;
use App\User;
use App\UserRoles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Helper;




class LoginService
{

    private  $user;
    private  $userRoles;
    private $mail;

    public function __construct(User $user,UserRoles $userRoles, MailService $mailService)
    {
        $this->user = $user;
        $this->userRoles = $userRoles;
        $this->mail = $mailService;
    }

    public function authenticateUser($data)
    {

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            return $user;
        }
        return false;
    }

    public function checkAdmin($data)
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            $userRoles = $this->getUserRole($user['user_role_id']);
            Session::put('userRoles',$userRoles);
            Session::put('users',$user);
            $location = $this->getLocationDetails(session()->get('users')->location_id);
            Session::put('locations',$location);
            $getWardTableID = $this->getWardTableNumber(session()->get('users')->ward_no);
            Session::put('wardTableID',$getWardTableID);

            return $user;
        }

    }

    public function registerUser($data)
    {
         Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = $this->user->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return compact('user');

    }

    public function logout()
    {
        session_start();
        Auth::logout();
        session_destroy();
    }

    public function deleteAccount($userId)
    {
        try{
            $this->user->where(['id'=>$userId])->delete();
            $this->logout();
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    public function getUserRole($id)
    {
        $userRoles = $this->userRoles->where('id',$id)->get()->first();
        return $userRoles;

    }

    public function getLocationDetails($id)
    {   $locationDetails =  DB::table('locations')->where('ID', $id)->get()->first();
        //$userRoles = $this->userRoles->where('id',$id)->get()->first();
        //$locationDetails = DB::select(DB::raw('SELECT * FROM locations where ID= 7'));
       // dd($locationDetails);
        return $locationDetails;

    }
    public function  getWardTableNumber($id){

        $getWardTableID =  DB::table('wards')->where('ward_no', $id)->get()->first();

        return $getWardTableID;
    }


}