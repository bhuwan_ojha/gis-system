<?php

namespace App\Services;


use App\Pages;
use Illuminate\Support\Facades\DB;

class PagesService
{
    public function __construct(Pages $pages)
    {
        $this->pages = $pages;
    }

    public function saveUpdate($pagesData)
    {
        DB::beginTransaction();
        try {
            $this->pages->create($pagesData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updatepages($pagesData)
    {
        try {
            $updatepages = $this->pages->where(['id' => $pagesData['id']])->update($pagesData);
            return $updatepages;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getMypages($pagesId =null,$page=null)
    {
        $pagesList = $this->pages
            ->where('created_by',$pagesId)
            ->orderBy('id','ASC')->paginate($page);
        return $pagesList;
    }

    public function getBypagesId($id)
    {
        $pages = $this->pages->find($id);
        return $pages;
    }

    public function delete($id)
    {
        $deleteUser = $this->pages->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllpages($page=null,$role= '')
    {
        $pagesList = $this->pages->orderBy('id','ASC')->paginate($page);

       // var_dump($pagesList);
       // exit;

        /*if($role == ''){

            $pagesList = $this->pages->select('wards.*','pages.*')->join('wards', 'wards.id', '=', 'pages.pages_of_ward_no')->paginate($page);

        }
        else if ($role == 'superadmin') {

            $pagesList = $this->pages->orderBy('pages.id','ASC')->join('wards', 'wards.id', '=', 'pages.pages_of_ward_no')->paginate($page);

        }
        else if ($role == 'admin') {

            $pagesList = $this->pages->orderBy('pages.id','ASC')->join('wards', 'wards.id', '=', 'pages.pages_of_ward_no')->where('wards.ward_no', session()->get('users')->ward_no)->paginate($page);
        }
        else if ($role == 'manager') {

            $pagesList = $this->pages->orderBy('pages.id','ASC')->join('wards', 'wards.id', '=', 'pages.pages_of_ward_no')->where('pages.created_by', session()->get('users')->id)->paginate($page);
        }
         */
        return $pagesList;
    }



    public function getWardpagesById($wardId,$pagesId)
    {
        $pages = $this->pages->where('pages_of_ward_no',$wardId)->find($pagesId);
        return $pages;
    }

    public function getMorepages($wardId,$pagesId,$page=null)
    {
        $pages = $this->pages->where('pages_of_ward_no',$wardId)
            ->where('id','!=',$pagesId)
            ->paginate($page);
        return $pages;
    }


    public function getMenu(){

        return DB::table('pages')->get();

    }

    public  function getBySlug($slug){

            //$pageData = $this->pages->where('slug',$slug)->get();
            $pageData = $this->pages->where('page_slug','=',$slug)->get();
            return $pageData;

    }

    public function getAllSlug($pageID = null){

        if($pageID != NULL) {
           return $this->pages->where('id','!=',$pageID)->get();
        }
        return $this->pages->get();
    }
}
