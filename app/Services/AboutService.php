<?php

namespace App\Services;

use App\Booking;
use App\Media;
use App\MediaCategory;
use App\About;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;

class AboutService
{
    public function __construct(About $about)
    {
        $this->about = $about;
    }

    public function saveUpdate($data)
    {
        try {
            if ($data[0]['ID']) {
                foreach ($data as $media) {
                    $updatemedia = $this->about->where('id', $media['ID'])->update($media);
                }
                return $updatemedia;
            }

            foreach ($data as $media) {
                $savemedia = $this->about->create($media);
            }
            return $savemedia;

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function getByMediaId($id)
    {
        $media = $this->about->find($id);
        return $media;
    }

    public function delete($id)
    {
        $deleteMedia = $this->about->where('id', $id)->delete();
        return $deleteMedia;
    }


    public function getAllImages($page = null)
    {
        $mediaList = $this->about
            ->where('about_us.media_image','!=',NULL)->orderBy('id', 'DESC')->paginate($page);
        return $mediaList;
    }

    public function getAllVideos($page = null)
    {
        $mediaList = $this->about
            ->where('about_us.video_url','!=',NULL)->orderBy('id', 'DESC')->paginate($page);
        return $mediaList;
    }
}