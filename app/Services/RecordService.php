<?php

namespace App\Services;


use App\Record;
use Illuminate\Support\Facades\DB;

class RecordService
{
    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    public function saveUpdate($recordData)
    {
        DB::beginTransaction();
        try {
            $this->record->create($recordData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateRecord($recordData)
    {
        try {
                $updateRecord = $this->record->where(['id' => $recordData['id']])->update($recordData);
                return $updateRecord;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByRecordId($id)
    {
        $user = $this->record->find($id);
        return $user;
    }

    public function delete($id)
    {
        $deleteUser = $this->record->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllRecord($page=null,$role= null)
    {


        if($role == ''){

            $userList = $this->record->orderBy('id','ASC')->paginate($page);

        }
        else if ($role == 'superadmin') {

            $userList = $this->record->orderBy('id','ASC')->paginate($page);

        }
        else if ($role == 'admin') {

            $userList = $this->record->where('added_by', session()->get('users')->id)->orderBy('id', 'ASC')->paginate($page);

        }
        else if ($role == 'manager') {

                $userList = $this->record->where('added_by', session()->get('users')->id)->orderBy('id','ASC')->paginate($page);
        }



        return $userList;
    }

    public function getRecordByCitizenshipNumber($citizenship_no)
    {
        $userdata = $this->record->where('citizenship_number','=',$citizenship_no)->first();
        return $userdata;
    }

    public function getMyRecord($recordId =null,$page=null)
    {
        $userList = $this->record
            ->where('id',$recordId)
            ->orderBy('id','ASC')->paginate($page);
        return $userList;
    }


    function countTotalPopulation()
    {
     return $this->record->count();
    }
    function countTotalOld()
    {
     return $this->record->where('age','>=',65)->count();
    }
    function countTotalWidow()
    {
     return $this->record->where('marital_status','=','Widowed')
         ->where('gender','=','Female')->count();
    }
    function countTotalPhysicalDisabled()
    {
     return $this->record->where('physical_disabled','=',1)->count();
    }


    function getLocationData($name,$categoryList){

        $additionSql = '';
        $category = json_decode($categoryList);


        if(!empty($category)){

            $categoryListAll =  "'" . implode('\',\'',$category)."'";

            $additionSql = " AND property_category IN ( $categoryListAll) ";
        }

        $pdo = DB::connection()->getPdo();

        $sql = "SELECT * FROM (

SELECT personal.`citizenship_number` as Citizen,personal.`blood_group` as BloodGroup,'Private Property' AS property_category,PR.created_at,kitta_number,coordinate, full_name,property_image,property_location_state,property_location_district,property_location_municipality FROM `property_records` PR  JOIN `personal_records` personal ON personal.`citizenship_number` = PR.`citizenship_number`
UNION ALL

SELECT '' as Citizen ,'' as BloodGroup ,`property_category`,created_at,kitta_number,IFNULL(coordinate,0) AS coordinate,property_title AS full_name,property_image,property_location_state,property_location_district,property_location_municipality FROM `public_property_records` ) p WHERE kitta_number LIKE '%$name%' OR
  full_name LIKE '%$name%' OR BloodGroup LIKE '%$name%' OR Citizen LIKE '%$name%' $additionSql";



        $sqlQuery = $pdo->query($sql);

        $data = $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

        return json_encode($data);

    }

    function getAllCategoryData(){


        $pdo = DB::connection()->getPdo();

        $sqlQuery = $pdo->query("SELECT * FROM (

SELECT 'Private Property' AS property_category,kitta_number,IFNULL(coordinate,0) AS coordinate, full_name FROM `property_records` PR  JOIN `personal_records` personal ON personal.`citizenship_number` = PR.`citizenship_number`
UNION ALL

SELECT `property_category`,kitta_number,IFNULL(coordinate,0) AS coordinate,property_title AS full_name FROM `public_property_records` ) p ");

        $data = $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

        return $data;


    }
    function getAllHeadOfFamily(){
        
        $pdo = DB::connection()->getPdo();

        $sqlQuery = $pdo->query("SELECT * FROM `personal_records` WHERE  head_of_family = 1 ");

        $data = $sqlQuery->fetchAll(\PDO::FETCH_OBJ);

        return $data;


    }

}