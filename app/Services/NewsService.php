<?php

namespace App\Services;


use App\News;
use Illuminate\Support\Facades\DB;

class NewsService
{
    public function __construct(News $news)
    {
        $this->news = $news;
    }

    public function saveUpdate($newsData)
    {
        DB::beginTransaction();
        try {
            $this->news->create($newsData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateNews($newsData)
    {
        try {
            $updateNews = $this->news->where(['id' => $newsData['id']])->update($newsData);
            return $updateNews;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getMyNews($newsId =null,$page=null)
    {
        $newsList = $this->news
            ->where('created_by',$newsId)
            ->orderBy('id','ASC')->paginate($page);
        return $newsList;
    }

    public function getByNewsId($id)
    {
        $news = $this->news->find($id);
        return $news;
    }

    public function getWardNewsById($wardId,$newsId)
    {
        $news = $this->news->where('news_of_ward_no',$wardId)->find($newsId);
        return $news;
    }



    public function delete($id)
    {
        $deleteUser = $this->news->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllNews($page=null,$role = null)
    {
        if($role == null){

            $newsList = $this->news->select('news.*')->orderBy('news.id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);

        }

        else if ($role == 'superadmin') {

            $newsList = $this->news->select('news.*')->orderBy('news.id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);

        }
         else if ($role == 'admin') {

             $newsList = $this->news->select('news.*')->orderBy('news.id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->where('wards.ward_no', session()->get('users')->ward_no)->paginate($page);
         }
         else if ($role == 'manager') {

             $newsList = $this->news->select('news.*')->orderBy('news.id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->where('news.created_by', session()->get('users')->id)->paginate($page);
         }

        // dd($newsList);
       // $newsList =  Store::selectRaw(' SELECT * FROM news')->paginate($page);
            //var_dump($newsList);

        //dd($newsList);
        //$all_transactions = DB::select(DB::raw('SELECT * FROM news')); //$query is raw SQL query say SELECT * FROM a LEFT JOIN b on a.id = b.a_id GROUP BY a.id
        //$newsList = Paginator::make($all_transactions, count($all_transactions), 10);
       // var_dump($newsList);
        //exit;

       // dd( DB::table('news')->get());
       // $results = DB::select( DB::raw("SELECT * FROM news") );
           // dd($newsList);
        //exit;
        return $newsList;
    }

    public function getAllNewsByWardId($wardId,$page=null)
    {
        $newsList = $this->news->where('news_of_ward_no',$wardId)->orderBy('id','DESC')->paginate($page);
        return $newsList;
    }

    public function getMoreNews($wardId,$newsId,$page=null)
    {
        $news = $this->news->where('news_of_ward_no',$wardId)
            ->where('id','!=',$newsId)
                ->paginate($page);
        return $news;
    }
}