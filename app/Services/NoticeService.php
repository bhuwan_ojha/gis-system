<?php

namespace App\Services;


use App\Notice;
use Illuminate\Support\Facades\DB;

class NoticeService
{
    public function __construct(Notice $notice)
    {
        $this->notice = $notice;
    }

    public function saveUpdate($noticeData)
    {
        DB::beginTransaction();
        try {
            $this->notice->create($noticeData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateNotice($noticeData)
    {
        try {
            $updateNotice = $this->notice->where(['id' => $noticeData['id']])->update($noticeData);
            return $updateNotice;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getMyNotice($noticeId =null,$page=null)
    {
        $noticeList = $this->notice
            ->where('created_by',$noticeId)
            ->orderBy('id','ASC')->paginate($page);
        return $noticeList;
    }

    public function getByNoticeId($id)
    {
        $notice = $this->notice->find($id);
        return $notice;
    }

    public function delete($id)
    {
        $deleteUser = $this->notice->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllNotice($page=null,$role= '')
    {
        $noticeList = $this->notice->orderBy('id','ASC')->paginate($page);

        if($role == ''){

            $noticeList = $this->notice->select('notice.*')->join('wards', 'wards.id', '=', 'notice.notice_of_ward_no')->paginate($page);

        }
        else if ($role == 'superadmin') {

            $noticeList = $this->notice->select('notice.*')->orderBy('notice.id','ASC')->join('wards', 'wards.id', '=', 'notice.notice_of_ward_no')->paginate($page);

        }
        else if ($role == 'admin') {

            $noticeList = $this->notice->select('notice.*')->orderBy('notice.id','ASC')->join('wards', 'wards.id', '=', 'notice.notice_of_ward_no')->where('wards.ward_no', session()->get('users')->ward_no)->paginate($page);
        }
        else if ($role == 'manager') {

            $noticeList = $this->notice->select('notice.*')->orderBy('notice.id','ASC')->join('wards', 'wards.id', '=', 'notice.notice_of_ward_no')->where('notice.created_by', session()->get('users')->id)->paginate($page);
        }
        return $noticeList;
    }



    public function getAllNoticeByWardId($wardId,$page=null)
    {
        $noticeList = $this->notice->where('notice_of_ward_no',$wardId)->orderBy('id','DESC')->paginate($page);
        return $noticeList;
    }

    public function getWardNoticeById($wardId,$noticeId)
    {
        $notice = $this->notice->where('notice_of_ward_no',$wardId)->find($noticeId);
        return $notice;
    }

    public function getMoreNotice($wardId,$noticeId,$page=null)
    {
        $notice = $this->notice->where('notice_of_ward_no',$wardId)
            ->where('id','!=',$noticeId)
            ->paginate($page);
        return $notice;
    }
}
