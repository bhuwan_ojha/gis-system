<?php

namespace App\Services;

use App\Booking;
use App\Media;
use App\MediaCategory;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;

class MediaService
{
    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    public function saveUpdate($data)
    {
        try {
            if ($data[0]['ID']) {
                foreach ($data as $media) {
                    $updatemedia = $this->media->where('id', $media['ID'])->update($media);
                }
                return $updatemedia;
            }

            foreach ($data as $media) {
                $savemedia = $this->media->create($media);
            }
            return $savemedia;

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function getByMediaId($id)
    {
        $media = $this->media->find($id);
        return $media;
    }

    public function delete($id)
    {
        $deleteMedia = $this->media->where('id', $id)->delete();
        return $deleteMedia;
    }


    public function getAllImages($page = null)
    {
        $mediaList = $this->media
            ->where('media.media_image','!=',NULL)->orderBy('id', 'DESC')->paginate($page);
        return $mediaList;
    }

    public function getAllVideos($page = null)
    {
        $mediaList = $this->media
            ->where('media.video_url','!=',NULL)->orderBy('id', 'DESC')->paginate($page);
        return $mediaList;
    }
}