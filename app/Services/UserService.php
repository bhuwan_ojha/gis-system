<?php

namespace App\Services;

use App\User;
use App\UserRoles;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;

class UserService
{
    public function __construct(User $user, MailService $mailService, UserRoles $roles)
    {
        $this->user = $user;
        $this->mail = $mailService;
        $this->roles = $roles;
    }

    public function saveUpdate($userData, $id = null)
    {
        DB::beginTransaction();
        try {
           $saveUser = $this->user->create($userData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateUser($userData)
    {
        try {
                $updateuser = $this->user->where(['id' => $userData['id']])->update($userData);
                return $updateuser;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByUserId($id)
    {
        $user = $this->user->find($id);
        return $user;
    }

    public function delete($id)
    {
        $deleteUser = $this->user->where('id',$id)->delete();
        return $deleteUser;
    }

    public function activate($userId)
    {
        $activateUser =  $this->user->where(['id'=>$userId])->update(['status'=>'Active']);
        return $activateUser;
    }

    public function deactivate($userId)
    {
        $deactivateUser =  $this->user->where(['id'=>$userId])->update(['status'=>'Inactive']);
        return $deactivateUser;
    }




    public function getAllUsers($page=null,$role = NULL)
    {
        if($role == NULL) {
            $userList = $this->user
                ->select('locations.*','user_roles.*','users.*')
                ->join('locations','locations.id','=','users.location_id')
                ->join('user_roles','user_roles.id','users.user_role_id')
                ->orderBy('users.id','ASC')->paginate($page);

        }

        else if ($role == 'superadmin') {

            //  $newsList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);
            //$newsList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);
            $userList = $this->user
                ->select('locations.*','user_roles.*','users.*')
                ->join('locations','locations.id','=','users.location_id')
                ->join('user_roles','user_roles.id','users.user_role_id')
                ->orderBy('users.id','ASC')->paginate($page);

        }
        else if ($role == 'admin') {

            $userList = $this->user
                ->select('locations.*','user_roles.*','users.*')
                ->join('locations','locations.id','=','users.location_id')
                ->join('user_roles','user_roles.id','users.user_role_id')->where('users.ward_no', session()->get('users')->ward_no)
                ->orderBy('users.id','ASC')->paginate($page);
        }
        else if ($role == 'manager') {

            $userList = $this->user
                ->select('locations.*','user_roles.*','users.*')
                ->join('locations','locations.id','=','users.location_id')
                ->join('user_roles','user_roles.id','users.user_role_id')->where('users.ward_no', session()->get('users')->ward_no)
                ->orderBy('users.id','ASC')->paginate($page);
        }

        return $userList;
    }

    public function getAllUsersByLocationId($location_id,$page=null)
    {
        $userList = $this->user
            ->select('locations.*','user_roles.*','users.*')
            ->join('locations','locations.id','=','users.location_id')
            ->join('user_roles','user_roles.id','users.user_role_id')
            ->where('locations.id','=',$location_id)
            ->orderBy('users.id','ASC')->paginate($page);
        return $userList;
    }

    public function getUserRoles($page=null)
    {
        $userList = $this->roles
            ->where('user_roles','!=','superadmin')
            ->orderBy('id','ASC')->paginate($page);
        return $userList;
    }

}