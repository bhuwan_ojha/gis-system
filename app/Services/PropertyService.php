<?php

namespace App\Services;


use App\Property;
use Illuminate\Support\Facades\DB;

class PropertyService
{
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    public function saveUpdate($propertyData)
    {
        DB::beginTransaction();
        try {
           $savePropertyData =  $this->property->create($propertyData);

            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }



    public function updateProperty($propertyData)
    {
        try {

                $updateProperty = $this->property->where(['id' => $propertyData['id']])->update($propertyData);
                return $updateProperty;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByPropertyId($id)
    {
        $user = $this->property->find($id);
        return $user;
    }
    public function getCoordinateByPropertyId($id)
    {
        $user = $this->coordinate->where('property_id','=',$id)->get();
        return $user;
    }

    public function delete($id)
    {
        $deleteUser = $this->property->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllProperty($page=null,$role = null)
    {
        if($role == NULL) {
            $userList = $this->property->orderBy('id', 'ASC')->paginate($page);
        }
        else if ($role == 'superadmin') {

          //  $newsList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);
            //$newsList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'news.news_of_ward_no')->paginate($page);
            $userList = $this->property->orderBy('id', 'ASC')->paginate($page);

        }
        else if ($role == 'admin') {

            $userList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'property.property_location_ward_no')->where('property_location_ward_no', session()->get('users')->ward_no)->paginate($page);
        }
        else if ($role == 'manager') {

            $userList = $this->property->orderBy('id','ASC')->join('wards', 'wards.id', '=', 'property.property_location_ward_no')->where('property.created_by', session()->get('users')->id)->paginate($page);
        }
        return $userList;
    }

    public function getMyProperty($propertyId =null,$page=null)
    {
        $userList = $this->property
            ->where('id',$propertyId)
            ->orderBy('id','ASC')->paginate($page);
        return $userList;
    }

}