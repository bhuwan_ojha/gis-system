<?php

namespace App\Services;


use App\About;
use App\Location;
use App\Ward;
use Illuminate\Support\Facades\DB;


class LocationService
{
    public function __construct(Location $location,Ward $ward)
    {
        $this->location = $location;
        $this->ward = $ward;
        $this->about = new About();
    }

    public function saveUpdate($locationData,$wardData)
    {
        DB::beginTransaction();
        try {
            $location = $this->location->create($locationData);
            foreach ($wardData as $ward) {
                $this->ward->create($ward);
            }

            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateLocation($locationData,$wardData)
    {
        try {
                $updateLocation = $this->location->where(['id' => $locationData['id']])->update($locationData);
             foreach ($wardData as $ward) {
                 if ($ward['id'] !=="") {
                     $this->ward->where(['id' => $ward['id']])->update($ward);
                 }else{
                     $this->ward->create($ward);
                 }
            }
                //return $updateLocation;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByLocationId($id)
    {
        $user = $this->location->find($id);
        return $user;
    }

    public function getWardByLocationId($location_id= NULL)
    {
        if($location_id != NULL) {
            $wards = $this->ward->where('location_id', $location_id)->get();
        } else {

            $wards = $this->ward->get();

        }
        return $wards;
    }

    public function delete($id)
    {
        $deleteUser = $this->location->where('id',$id)->delete();
        return $deleteUser;
    }


    public function getAllLocation($page=null)
    {
        $userList = $this->location->orderBy('id','ASC')->paginate($page);

        return $userList;
    }

    public function countTotalWards()
    {
        $wardList = $this->ward->orderBy('id','ASC')->count();
        return $wardList;
    }

    public function getMyLocation($locationId =null,$page=null)
    {
        $userList = $this->location
            ->where('id',$locationId)
            ->orderBy('id','ASC')->paginate($page);
        return $userList;
    }
    public function getMyWard($locationId =null,$page=null)
    {
        $userList = $this->ward
            ->where('location_id',$locationId)
            ->orderBy('id','ASC')->paginate($page);
        return $userList;
    }

    public function getAllWards()
{
    $wardList = $this->ward->get();
    return $wardList;
}

    public function getAboutUS()
    {
        $aboutList = $this->about->get();
        return $aboutList;
    }
    public function deleteWardByID($id)
    {
        $deleteUser = $this->ward->where('id',$id)->delete();
        return $deleteUser;
    }
}