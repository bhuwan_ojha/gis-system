
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login' , function () {
    return view('admin.login');
});

Route::group(['prefix' => 'admin/users',['middleware'=>'web']], function () {
    Route::get('/','Admin\UserController@index');
    Route::get('/profile','Admin\UserController@profile');
    Route::get('/add-user','Admin\UserController@userForm');
    Route::get('/{id}/edit-user','Admin\UserController@userForm');
    Route::post('/add','Admin\UserController@saveUpdateUser');
    Route::post('/profile','Admin\UserController@updateProfile');
    Route::get('/{id}/edit-user','Admin\UserController@userForm');
    Route::get('/{id}/delete','Admin\UserController@deleteUser');
    Route::post('/saveUpdate','Admin\UserController@saveUpdateUser');
    Route::get('/{id}/active','Admin\UserController@activateUser');
    Route::get('/{id}/inactive','Admin\UserController@deactivateUser');
});


Route::group(['prefix' => 'admin/locations',['middleware'=>'web']], function () {
    Route::get('/','Admin\LocationController@index');
    Route::get('/add-location','Admin\LocationController@locationForm');
    Route::get('/{id}/edit-location','Admin\LocationController@locationForm');
    Route::post('/add','Admin\LocationController@saveUpdateLocation');
    Route::get('/{id}/delete','Admin\LocationController@deleteLocation');
    Route::post('/update','Admin\LocationController@saveUpdateLocation');
    Route::post('/deleteWardData','Admin\LocationController@deleteWardData');
});

Route::group(['prefix' => 'admin/records',['middleware'=>'web']], function () {
    Route::get('/','Admin\RecordController@index');
    Route::get('/add-record','Admin\RecordController@recordForm');
    Route::get('/{id}/edit-record','Admin\RecordController@recordForm');
    Route::post('/add','Admin\RecordController@saveUpdateRecord');
    Route::get('/getRecordByCitizenshipNumber','Admin\RecordController@getRecordByCitizenshipNumber');
    Route::get('/{id}/delete','Admin\RecordController@deleteRecord');
    Route::post('/update','Admin\RecordController@saveUpdateRecord');
});

Route::group(['prefix' => 'admin/news',['middleware'=>'web']], function () {
    Route::get('/','Admin\NewsController@index');
    Route::get('/add-news','Admin\NewsController@newsForm');
    Route::get('/{id}/edit-news','Admin\NewsController@newsForm');
    Route::post('/add','Admin\NewsController@saveUpdateNews');
    Route::get('/{id}/delete','Admin\NewsController@deleteNews');
    Route::post('/update','Admin\NewsController@saveUpdateNews');
});
Route::group(['prefix' => 'admin/notice',['middleware'=>'web']], function () {
    Route::get('/','Admin\NoticeController@index');
    Route::get('/add-notice','Admin\NoticeController@noticeForm');
    Route::get('/{id}/edit-notice','Admin\NoticeController@noticeForm');
    Route::post('/add','Admin\NoticeController@saveUpdateNotice');
    Route::get('/{id}/delete','Admin\NoticeController@deleteNotice');
    Route::post('/update','Admin\NoticeController@saveUpdateNotice');
});

Route::group(['prefix' => 'admin/Pages',['middleware'=>'web']], function () {
    Route::get('/','Admin\PagesController@index');
    Route::get('/add-pages','Admin\PagesController@pagesForm');
    Route::get('/{id}/edit-pages','Admin\PagesController@pagesForm');
    Route::post('/add','Admin\PagesController@saveUpdatepages');
    Route::get('/{id}/delete','Admin\PagesController@deletepages');
    Route::post('/update','Admin\PagesController@saveUpdatepages');
});

Route::group(['prefix' => 'admin/public-property',['middleware'=>'web']], function () {
    Route::get('/','Admin\PublicPropertyController@index');
    Route::get('/add-property','Admin\PublicPropertyController@propertyForm');
    Route::get('/{id}/edit-property','Admin\PublicPropertyController@propertyForm');
    Route::post('/add','Admin\PublicPropertyController@saveUpdateProperty');
    Route::get('/{id}/delete','Admin\PublicPropertyController@deleteProperty');
    Route::post('/update','Admin\PublicPropertyController@saveUpdateProperty');
});

Route::group(['prefix' => 'admin/property',['middleware'=>'web']], function () {
    Route::get('/','Admin\PropertyController@index');
    Route::get('/add-property','Admin\PropertyController@propertyForm');
    Route::get('/{id}/edit-property','Admin\PropertyController@propertyForm');
    Route::post('/add','Admin\PropertyController@saveUpdateProperty');
    Route::get('/{id}/delete','Admin\PropertyController@deleteProperty');
    Route::post('/update','Admin\PropertyController@saveUpdateProperty');
});

Route::group(['prefix' => 'admin/media',['middleware'=>'web']], function () {
    Route::get('/','Admin\MediaController@index');
    Route::get('/add-media','Admin\MediaController@mediaForm');
    Route::post('/add','Admin\MediaController@saveUpdateMedia');
    Route::get('/{id}/delete','Admin\MediaController@deleteMedia');
});

Route::group(['prefix' => 'admin/about',['middleware'=>'web']], function () {
    Route::get('/','Admin\AboutController@index');
    Route::get('/add-media','Admin\AboutController@mediaForm');
    Route::post('/add','Admin\AboutController@saveUpdateMedia');
    Route::get('/{id}/delete','Admin\AboutController@deleteMedia');
});


Route::group(['prefix' => 'admin',['middleware'=>'web']], function () {
    Route::post('/login-submit','Auth\LoginController@adminLogin');
    Route::get('/logout','Auth\LoginController@adminLogout');
    Route::get('/{slug}','Admin\PageController@getPage');
    Route::get('/' , function () {
        return view('admin.login');
    });
});



Route::post('/register','Auth\LoginController@postRegister');
Route::post('/authenticate','Auth\LoginController@Authenticate');
Route::post('/change-password','Auth\LoginController@changePassword');
Route::get('/logout','Auth\LoginController@logout');
Route::post('/delete-account','Auth\LoginController@deleteAccount');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');








Route::group(['prefix' => 'news'], function () {
    Route::get('/','NewsController@index');
    Route::get('/{ward}/news-list','NewsController@index');
    Route::get('/{ward}/{newsId}/news-detail','NewsController@getNewsDetail');
});

Route::group(['prefix' => 'notice'], function () {
    Route::get('/','NoticeController@index');
    Route::get('/{ward}/notice-list','NoticeController@index');
    Route::get('/{ward}/{noticeId}/notice-detail','NoticeController@getNoticeDetail');
});








Route::group(['prefix' => 'news'], function () {
    Route::get('/','NewsController@index');
    Route::get('/{ward}/news-list','NewsController@index');
    Route::get('/{ward}/{newsId}/news-detail','NewsController@getNewsDetail');
});

Route::group(['prefix' => 'notice'], function () {
    Route::get('/','NoticeController@index');
    Route::get('/{ward}/notice-list','NoticeController@index');
    Route::get('/{ward}/{noticeId}/notice-detail','NoticeController@getNoticeDetail');
});


Route::get('/getMapSearchResult','PageController@getMapSearchResult');
Route::post('/getMapSearchResult','PageController@getMapSearchResult');
Route::post('/sendMail','PageController@sendMail');

Route::get('/admin/getMapSearchResult','PageController@getMapSearchResult');
Route::post('/admin/getMapSearchResult','PageController@getMapSearchResult');

Route::get('/','PageController@getPage');
Route::get('/{slug}','PageController@getPage');


Route::get('/','PageController@getPage');
Route::get('/{slug}','PageController@getPage');




Auth::routes();

