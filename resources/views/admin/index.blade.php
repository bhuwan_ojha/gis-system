@php($nav = 1)
@extends('admin.layouts.layout')
@section('content')



    <div class="content-wrapper">


        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">गृहपृष्ट</span> - ड्यासबोर्ड</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <span class="breadcrumb-item active">ड्यासबोर्ड</span>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>




        <div class="content" >

            <div>
            <form action="" class="gis-form-wrapper">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <select class="form-control categoryListHome" id="sel1">
                                <option value="All">सबै <span class="caret"></span> </option>

                                @foreach($category as $key=>$location)


                                    @if($key =='Private Property' && session()->get('userRoles')->user_roles =='manager' )
                                        @continue
                                    @endif
                                    <option value="{{ $key }}">{{ $location }}</option>
                                @endforeach


                            </select>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group" id="sell2">
                            <input id="mapsearch" type="text" class="form-control" placeholder="यहाँ खोज्नुहोस्">
                        </div>
                    </div>

                </div>
                <div class="col-md-9 searchresult" style="width:50%;background: white;margin-top: -20px;margin-left: 20.5%;position: absolute;z-index: 2">
                    <div class="form-group"  >
                        <ul  style="list-style-type: none;padding-left: 0px;" id="searchfieldlist1">


                        </ul>
                    </div>
                </div>
            </form>
           </div>
            <div id="backendMap"></div>


        </div>
    </div>

    <script>

        var Categortdatalist = JSON.parse( '<?php  echo ($allCategoryLocationJsonData) ?>');

        $('#mapsearch').on('keyup', function(){


            var categoryList = [];
            /*
             $('#categoryLists input').each(function(){
             if($(this).is(':checked')){

             categoryList.push($(this).val());

             }
             })
             */
            if($('.categoryListHome').val() != 'All') {

                categoryList.push($('.categoryListHome').val());
            }

            var csrftoken =  $('meta[name="csrf-token"]').attr('content');
            var value = $(this).val();

            $.ajax({
                url: "getMapSearchResult",
                type: 'POST',
                headers: {'X-CSRF-TOKEN': csrftoken},
                data :{"name" :value,"categorylist":JSON.stringify(categoryList)},
                success: function(result){


                    var location_details = JSON.parse(result);
                    $('#searchfieldlist1').html('');
                    $.each( location_details, function( key  ) {
                        $('#searchfieldlist1').append( '<li data-toggle="modal" data-target="#modalView" style="cursor:pointer;margin-top:10px" value="'+location_details[key]["coordinate"]+'" fullname="'+location_details[key]["full_name"]+'" kittanumberhome="'+location_details[key]["kitta_number"]+'" class="kittanumberdataclass">'+location_details[key]['full_name'] +" (" + location_details[key]["kitta_number"]+')</li>')
                    });
                    if(location_details.length >0) { $('#searchfieldlist1').parents('.searchresult').show();} else { $('#searchfieldlist1').parents('.searchresult').hide() }

                    if(location_details.length == 0 && value!='' ){

                        $('#searchfieldlist1').append('<li style=";margin-top:10px">कुनै रेकर्ड फेला परेन</li>')
                        $('#searchfieldlist1').parents('.searchresult').show()
                    }

                }});

        })




    </script>



@endsection