@php($nav = 9)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">समाचार सम्पादन </span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">समाचार सम्पादन</a>
                        <span class="breadcrumb-item active">समाचार</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/news/add" method="post" id="location-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">समाचार सम्पादन</legend>
                            <input type="hidden" class="form-control" name="id" value="{{$news->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">समाचार शीर्षक <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" required autocomplete="off" name="news_title" value="{{$news->news_title or ''}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">समाचार वर्ग <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <select  name="news_category" required class="form-control">
                                        <option value="">समाचार वर्ग चयन गर्नुहोस्</option>
                                        @php($newsList = \App\Libraries\TMHelper::getAllNewsCategory())
                                        @foreach($newsList as $key=>$newsCategory )
                                            <option @if(isset($news->news_category) && $news->news_category == $newsCategory) selected @endif value="{{$newsCategory}}">{{$key}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड नं <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <select  name="news_of_ward_no" required class="form-control">
                                        <option value="">वार्ड नम्बर चयन गर्नुहोस्</option>
                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                        @foreach($wards as $ward)
                                            <option @if(isset($news->news_of_ward_no) && $news->news_of_ward_no == $ward->id) selected @endif value="{{$ward->id}}">{{env('SITE_NAME')}} वडा न. {{$ward->ward_no}}</option>
                                        @endforeach

                                        @else

                                            <option  selected  value="{{session()->get('wardTableID')->id}}">{{env('SITE_NAME')}} वडा न. {{session()->get('users')->ward_no}}</option>

                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">समाचार विवरण <span class="required-astrik">*</span></label>
                                <div class="col-lg-12">
                                    <textarea type="text" required class="form-control"  id="editor" autocomplete="off" name="news_description" value="{{$news->news_description or ''}}">{{$news->news_description or ''}}</textarea>
                                </div>
                            </div>

                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection