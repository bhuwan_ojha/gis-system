@php($nav = 0)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">प्रोफाईल सम्पादन गर्नुहोस्</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">प्रोफाईल</a>
                        <span class="breadcrumb-item active">प्रोफाईल सम्पादन गर्नुहोस्</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="card">
                <div class="card-body">
                    <form action="/admin/users/profile" method="post" id="booking-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">प्रोफाईल सम्पादन गर्नुहोस्</legend>
                            <input type="hidden" class="form-control" name="id" value="{{$user->id or ''}}">
                            @include('flash')
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">पहिलो नाम</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" required name="first_name" value="{{$user->first_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">थर</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" required name="last_name" value="{{$user->last_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">इमेल</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" required name="email" value="{{$user->email or ''}}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">नया पासवर्ड</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" required name="new_password" placeholder="********">
                                </div>
                            </div>

                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection