@php($nav = 920)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Abouts Us  Upload</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Edit About Us ( Images )</h4>
                    <div class="card">

                        <form action="/admin/about/add" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="media-upload-wrapper">
                            <div class="form-wrapper">

                                <div class="row">
                                    @foreach($images as $media)
                                    <div class="col-md-4">
                                        <input type="hidden" name="ID[]" value="{{$media->id}}">

                                        <div class="form-group">
                                            <div class="">
                                                <img class="new-img-preview" src="/uploads/about/{{$media->media_image}}" title="">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> Replace Image</span>
                                                        <input  onchange="readURL(this)" name="media_image[]" type="file" class="attachment_upload">
                                                    </div>

                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-danger" onclick="return(confirm('Are you sure want to delete this media file?'));" href="/admin/about/{{$media->id}}/delete">Delete Image</a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                            <div class="ancor-division">
                                <button type="submit" class="btn btn-success"><span class="pull-right icon-arrow-right16"></span>  EDIT & UPDATE ALL </button>
                            </div>
                        </div>
            </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script>
        $(document).ready(function() {
            var brand = document.getElementById('new-img-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.new-img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#new-img-id").change(function() {
                readURL(this);
            });
        });
    </script>

    <!-- bootsrap modal for video edit -->

    <script>
        function readURL(thisObj) {
            debugger;
            if (thisObj.files && thisObj.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    debugger;
                    $(thisObj).parent().parent().parent().prev().find('img')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(thisObj.files[0]);
            }
        }



    </script>
@endsection