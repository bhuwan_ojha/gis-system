@php($nav = 5)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">स्थान सम्पादन गर्नुहोस्</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">स्थान सूची</a>
                        <span class="breadcrumb-item active">स्थान सम्पादन</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/locations/add" method="post" id="location-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">स्थान सम्पादन</legend>
                            <input type="hidden" class="form-control"  name="id" value="{{$locations->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">नाम <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" autocomplete="off" required name="name" value="{{$locations->name or ''}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">प्रदेश <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <select  required name="state" class="form-control">
                                        <option value="">प्रदेश चयन गर्नुहोस्</option>
                                        @php($stateList = \App\Libraries\TMHelper::getAllStates())
                                        @foreach($stateList as $state )
                                            <option @if(isset($locations) && $locations->state == $state) selected @endif value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">जिल्ला <span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <select  required name="district"  class="form-control">
                                        <option value="">जिल्ला चयन गर्नुहोस्</option>
                                        @php($districtList = \App\Libraries\TMHelper::getAllDistrict())
                                        @foreach($districtList as $district )
                                            <option @if(isset($locations) && $locations->district == $district) selected @endif value="{{$district}}">{{$district}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="wards">
                                @if(isset($wards) && count($wards)> 0)
                                    @foreach($wards as $ward)
                                        <div class="ward-add">
                                        <input type="hidden"  name="ward_id[]" value="{{$ward->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड विवरणहरू <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" min="1" class="form-control" placeholder="वार्ड नम्बर" autocomplete="off" required name="ward_no[]" value="{{$ward->ward_no or '1'}}">
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" placeholder="वार्ड ठेगाना" autocomplete="off" required name="ward_address[]" value="{{$ward->ward_address or ''}}">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड कार्यालय फोटो</label>
                                <div class="col-lg-6">
                                    <input type="file" class="form-control" autocomplete="off"  name="ward_image[]" value="{{$ward->ward_image or ''}}">
                                </div>
                            </div>
                                        @if(isset($ward->ward_image))
                                            <div class="form-group row">
                                                <div class="col-lg-3">
                                                    <img src="/uploads/ward/{{$ward->ward_image}}" width="100px;">
                                                </div>
                                            </div>
                                            <div class="col-md-8 delete-wards text-right"><button type="button" class="btn btn-danger" onclick="removeThisWards(this)">मेटाउनुहोस् <i class="icon-trash ml-2"></i></button></div>
                                        @endif
                                        </div>
                                    @endforeach
                                    @else
                                    <div class="ward-add">
                                    <div class="form-group row">
                                        <label class="col-form-label font-weight-bold col-lg-2">वार्ड विवरणहरू <span class="required-astrik">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" min="1" class="form-control" placeholder="वार्ड नम्बर" autocomplete="off" required name="ward_no[]" value="{{$wards->ward_no or '1'}}">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control" placeholder="वार्ड ठेगाना" autocomplete="off" required name="ward_address[]" value="{{$wards->ward_address or ''}}">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label font-weight-bold col-lg-2">वार्ड कार्यालय फोटो</label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" autocomplete="off" required name="ward_image[]" value="{{$wards->ward_image or ''}}">
                                        </div>
                                    </div>
                                    </div>

                                    @endif
                            </div>
                            <div class="text-left">
                                <button type="button" class="btn btn-success" onclick="addMoreWards(this)">अझै थप <i class="icon-plus-circle2 ml-2"></i></button>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    function removeThisWards(thisObj) {


        var wardId = $(thisObj).parents('.ward-add').find('input[type=hidden]').val();
        var csrftoken =  $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "/admin/locations/deleteWardData",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': csrftoken},
            data :{"wardID" :wardId},
            success: function(result){



            }});

        $(thisObj).parent().parent().remove();

        //
    }
</script>