@php($nav = 786)
@extends('admin.layouts.layout')
@section('content')
    <style>

    </style>
    <?php
            $sluglist = [];

            foreach($slugs as $slug) {
                array_push($sluglist,$slug->slug);
            }
            $sluglist = json_encode($sluglist);

    ?>
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Page Manager </span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">Page Manager</a>
                        <span class="breadcrumb-item active">Page Manager</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/Pages/add" method="post" id="page-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">

                            <input type="hidden" class="form-control" name="id" value="{{$pages->id or ''}}">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Page Title</legend>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Page Title<span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" id="page_title" class="form-control" autocomplete="off" required name="page_title" value="{{$pages->page_title or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Slug<span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" required class="form-control" autocomplete="off"  id="slug" required name="page_slug" value="{{$pages->page_slug or ''}}">
                                    <span id="slugmessage" style="color: red; font-size: 14px; display: none "> Invalid Slug or Slug Already Exits</span>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Page Description</label>
                                <div class="col-lg-12">
                                    <textarea type="text" class="form-control"  id="editor" autocomplete="off" required name="page_description" value="{{$pages->page_description or ''}}">{{$pages->page_description or ''}}</textarea>
                                </div>
                            </div>

                            {{--<div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Page Description 2</label>
                                <div class="col-lg-12">
                                    <textarea type="text" class="form-control"  id="editor1" autocomplete="off" required name="page_description_second" value="{{$pages->page_description_second or ''}}">{{$pages->page_description or ''}}</textarea>
                                </div>
                            </div>--}}
                            {{--<div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सूचना फाइल</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control"  id="editor" autocomplete="off"  name="page_media" value="{{$pages->page_media or ''}}">
                                </div>
                            </div>
                            @if(isset($notice->notice_media))
                                <div class="form-group row">
                                    <label class="col-form-label font-weight-bold col-lg-2">यहाँबाट सूचना सूचना फाइलहरू डाउनलोड गर्नुहोस्:</label>
                                    <div class="col-lg-3">
                                        <a class="btn btn-success" href="/uploads/pages/pages_files/{{$pages->page_media}}">डाउनलोड गर्न क्लिक गर्नुहोस्</a>
                                    </div>
                                </div>
                            @endif--}}


                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script>


    var allSlugList = JSON.parse('<?php echo $sluglist ?>');
    var slug = function(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
    }

    $('#page_title').on('keyup', function(){

        $('#slug').val(slug($(this).val()));
    })


    $('#slug').on('keyup', function(){
        var RegexTest = new RegExp('^[a-z0-9]+(?:-[a-z0-9]+)*$');

        var slug = $(this).val();

        if(RegexTest.test(slug)  && allSlugList.inArray(slug) == -1){

            $('button[type=submit]').removeAttr('disabled');

            $('#slugmessage').hide();

        } else {

            $('button[type=submit]').prop('disabled','disabled');
            $('#slugmessage').show();
        }

    })










</script>

@endsection