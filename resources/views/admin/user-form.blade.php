@php($nav = 7)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">प्रयोगकर्ता सम्पादन गर्नुहोस्</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">प्रयोगकर्ता</a>
                        <span class="breadcrumb-item active">प्रयोगकर्ता सम्पादन गर्नुहोस्</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/users/saveUpdate" method="post" id="user-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">प्रयोगकर्ता सम्पादन गर्नुहोस्</legend>
                            <input type="hidden" class="form-control"  name="id" value="{{$user->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">पहिलो नाम</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" autocomplete="off" required name="first_name" value="{{$user->first_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">थर</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" autocomplete="off" required name="last_name" value="{{$user->last_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">इमेल</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" autocomplete="off" required name="email" value="{{$user->email or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">पासवर्ड</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" autocomplete="off" required name="password" value="{{$user->password or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">स्थान</label>
                                <div class="col-lg-6">
                                    <select  required name="location_id" class="form-control required">
                                        <option value="">स्थान चयन गर्नुहोस्</option>
                                        @foreach($locations as $location)
                                        <option @if(isset($user) && $location->id == $user->location_id) selected @endif value="{{$location->id}}">{{$location->name}} - {{$location->district}} - {{$location->state}} </option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">स्थान</label>
                                <div class="col-lg-6">
                                    <select  required name="ward_no" class="form-control">
                                        <option value="">वार्ड नम्बर चयन गर्नुहोस्</option>

                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                            @foreach($wards as $ward)
                                                <option @if(isset($user) && $ward->ward_no == $user->ward_no) selected @endif value="{{$ward->ward_no}}">{{env('SITE_NAME')}} वार्ड नम्बर {{$ward->ward_no}}</option>
                                            @endforeach

                                        @else

                                            <option  selected  value="{{session()->get('users')->ward_no}}">{{env('SITE_NAME')}} वार्ड नम्बर {{session()->get('users')->ward_no}}</option>

                                        @endif
                                    </select>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">प्रयोगकर्ता भूमिका</label>
                                <div class="col-lg-6">
                                    <select  required name="user_role_id" class="form-control">
                                        <option value="">प्रयोगकर्ता भूमिका चयन गर्नुहोस्</option>
                                        @foreach($userRoles as $roles)
                                            <option @if(isset($user) && $roles->ID == $user->user_role_id) selected @endif value="{{$roles->ID}}">{{$roles->user_roles}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection