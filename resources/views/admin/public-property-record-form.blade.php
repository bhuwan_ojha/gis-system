@php($nav = 13)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">सार्वजनिक सम्पत्ति रेकर्ड सम्पादन गर्नुहोस्</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">सार्वजनिक सम्पत्ति रेकर्ड सम्पादन</a>
                        <span class="breadcrumb-item active">सार्वजनिक सम्पत्ति रेकर्ड</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/public-property/add" method="post" id="record-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">सार्वजनिक सम्पत्ति रेकर्ड सम्पादन</legend>
                            <input type="hidden" class="form-control"  name="id" value="{{$property->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सार्वजनिक सम्पत्ति वर्ग <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">

                                    <select class="form-control" required name="property_category" onchange="checkPropertyCategory(this)">
                                        <option value="">सम्पत्ति वर्ग चयन गर्नुहोस्</option>
                                        @php($propertyCategoryList = \App\Libraries\TMHelper::getAllPropertyCategory())
                                        @foreach($propertyCategoryList as $key=>$propertyCategory )
                                            <option <?php if(isset($property) && $property->property_category == $key)  { echo "selected"; } ?> value="{{$key}}">{{$propertyCategory}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">सम्पत्ति नाम <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" required name="property_title" value="{{$property->property_title or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">कित्ता नम्बर</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off"  name="kitta_number" value="{{$property->kitta_number or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">मोड नम्बर</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off"  name="mode_number" value="{{$property->mode_number or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">क्षेत्र(वर्गफल)</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off"  name="area" value="{{$property->area or ''}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">स्थायी प्रदेश <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select  required name="property_location_state" class="form-control">
                                        <option value="">प्रदेश चयन गर्नुहोस् </option>
                                        @php($stateList = \App\Libraries\TMHelper::getAllStates())
                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                            @foreach($stateList as $state )
                                                <option @if(isset($property) && $property->property_location_state == $state) selected @endif value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                        @else
                                            <option  selected value="{{session()->get('locations')->state}}">{{session()->get('locations')->state}}</option>
                                        @endif
                                    </select>
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">जिल्ला <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select  required name="property_location_district" class="form-control">
                                        <option value="">जिल्ला चयन गर्नुहोस्</option>
                                        @php($districtList = \App\Libraries\TMHelper::getAllDistrict())

                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                            @foreach($districtList as $district )
                                                <option @if(isset($property) && $property->property_location_district == $district) selected @endif value="{{$district}}">{{$district}}</option>
                                            @endforeach
                                        @else
                                            <option  selected  value="{{session()->get('locations')->district}}">{{session()->get('locations')->district}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">गाविस/नगरपालिका/उप-मेट्रो/महानगरीय <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    @if(session()->get('userRoles')->user_roles =='superadmin')
                                        <input type="text" class="form-control" autocomplete="off" required name="property_location_municipality" value="{{$property->property_location_municipality or ''}}">
                                    @else
                                        <input type="text" class="form-control" autocomplete="off" required readonly name="property_location_municipality" value="{{session()->get('locations')->name}}">
                                    @endif
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड नम्बर <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    @if(session()->get('userRoles')->user_roles =='superadmin')
                                        <input type="number" class="form-control" autocomplete="off" required name="property_location_ward_no" value="{{$property->property_location_ward_no or ''}}">
                                    @else
                                        <input type="number" class="form-control" autocomplete="off" required readonly name="property_location_ward_no" value="{{session()->get('users')->ward_no}}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सार्वजनिक सम्पत्ति नक्शा</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control" autocomplete="off"  name="property_map_image" value="{{$property->property_map_image or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">सम्पत्ति तस्वीर</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control" autocomplete="off"  name="property_image" value="{{$property->property_image or ''}}">
                                </div>
                            </div>

                                <div class="form-group row">
                                       <div class="col-lg-6">
                                           @if(isset($property->property_map_image))
                                           <img src="/uploads/property/{{$property->property_image}}" width="100px;">
                                           @endif
                                       </div>

                                       <div class="col-lg-6">
                                           @if(isset($property->property_image))
                                           <img src="/uploads/property/{{$property->property_image}}" width="100px;">
                                           @endif
                                       </div>
                                   </div>
                               <div class="coordinate">
                                   <div class="form-group row">
                                   <label class="col-form-label font-weight-bold col-lg-2">निर्देशांक(Coordinates)<span class="required-astrik">*</span></label>
                                   <div class="col-lg-3">
                                       <input type="text" class="form-control" placeholder="40.689263 -74.044505 " autocomplete="off" required name="coordinate" value="{{$property->coordinate or ''}}">
                                   </div>
                                   </div>
                               </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सम्पत्ति विवरण</label>
                                <div class="col-lg-12">
                                    <textarea type="text" class="form-control"  id="editor" autocomplete="off" required name="property_description" value="{{$property->property_description or ''}}">{{$property->property_description or ''}}</textarea>
                                </div>
                            </div>

                           </fieldset>
                           <div class="text-right">
                               <button type="submit" class="btn btn-primary">पेश गर्नुहोस्  <i class="icon-paperplane ml-2"></i></button>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   @endsection