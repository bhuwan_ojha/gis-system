@php($nav = 4)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">स्थान सूची</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">स्थान</a>
                        <span class="breadcrumb-item active">स्थान सूची</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            @include('flash')
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>क्रम सङ्ख्या</th>
                    <th>नाम</th>
                    <th>जिल्ला</th>
                    <th>प्रदेश</th>
                                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
                </tr>
                </thead>
                <tbody>
                @php($x=1)
                @foreach($locations as $location)
                <tr>
                    <td>{{$x++}}</td>
                    <td>{{$location->name}}</td>
                    <td><a href="#">{{$location->district}}</a></td>
                    <td>{{$location->state}}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/admin/locations/{{$location->id}}/edit-location" class="dropdown-item"><i class="icon-pencil"></i> सम्पादन गर्नुहोस्</a>
                                    <a href="/admin/locations/{{$location->id}}/delete" onclick="return(confirm('के तपाई पक्का यो स्थान हटाउन चाहनुहुन्छ? ?'))" class="dropdown-item"><i class="icon-trash"></i> मेटाउनुहोस्</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
            <div class="col-md-12 offset-6">{{ $locations->links() }}</div>
        </div>
@endsection