@php($nav = 3)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">रेकर्ड सम्पादन गर्नुहोस्</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">रेकर्ड</a>
                        <span class="breadcrumb-item active">रेकर्ड सम्पादन गर्नुहोस्</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/records/add" method="post" id="record-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">रेकर्ड सम्पादन गर्नुहोस्</legend>
                            <input type="hidden" class="form-control"  name="id" value="{{$records->id or ''}}">
                            <h2>व्यक्तिगत जानकारी:</h2>

                            @if(isset($records))
                            @php($name = explode(" ",$records->full_name))
                                @endif

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Head Of Family <span class="required-astrik"></span></label>
                                <div class="col-lg-3">
                                    <input value="1" <?php if( isset($records->head_of_family) && $records->head_of_family ==1) { echo 'checked';} ?> id="head_of_family" style="height: 20px;" type="checkbox" class="form-control" autocomplete="off"  name="head_of_family" value="">
                                </div>

                                <label class="col-form-label font-weight-bold col-lg-2">Select Head Of Family <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select class="form-control"  name="pariwar_no" >
                                        <option value="">Head चयन गर्नुहोस्</option>
                                        @foreach($headofFamily as $headofFamily )

                                            <option value="{{ $headofFamily->id }}" @if( ( isset($records->pariwar_no) && $records->pariwar_no == $headofFamily->id))  selected  @endif>{{ $headofFamily->full_name }}</option>
                                            @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">पहिलो नाम<span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" required name="first_name" value="{{$name[0] or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">मध्य नाम </label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off"  name="middle_name" value="{{isset($name[2]) ? $name[1] : ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">थर<span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" required name="last_name" value="{{isset($name[2]) ? $name[2] : (isset($name[1]) ? $name[1] : '')}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">जन्म मिति <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" id="dateofbirth" class="form-control" autocomplete="off" required  ">
                                    <input type="hidden" id="dateofbirthmain" class="form-control" autocomplete="off" required name="dob" value="{{$records->dob or ''}}">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">लिङ्ग <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select class="form-control" required name="gender" >
                                    <option value="">लिङ्ग चयन गर्नुहोस्</option>
                                        <option value="Male" @if(isset($records->gender)? $records->gender=="Male" : '')selected @endif>पुरुष</option>
                                        <option value="Female" @if(isset($records->gender)? $records->gender=="Female" : '')selected @endif>महिला</option>
                                        <option value="Others"@if(isset($records->gender)? $records->gender=="Others" : '')selected @endif>तेस्रो लिङ्ग</option>
                                    </select>
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">उमेर</label>
                                <div class="col-lg-3">
                                    <input type="number" class="form-control" autocomplete="off" id="age"  name="age" value="{{$records->age or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">जन्मस्थान <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text"  required name="birth_place" class="form-control" value="{{$records->birth_place or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">पति/पत्नीको नाम</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off"  name="husband_or_wife_name" value="{{$records->husband_or_wife_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">वैवाहिक स्थिति <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select  required name="marital_status" class="form-control">
                                        <option value="">वैवाहिक स्थिति चयन गर्नुहोस्</option>
                                        @php($maritalList = \App\Libraries\TMHelper::getAllMaritalStatus())
                                        @foreach($maritalList as $marital )
                                            <option @if(isset($records) && $records->marital_status == $marital) selected @endif value="{{$marital}}">{{$marital}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">रक्त समूह</label>
                                <div class="col-lg-3">
                                    <select   name="blood_group" class="form-control">
                                        <option value="">रक्त समूह चयन गर्नुहोस्</option>
                                        @php($bloodGroupList = \App\Libraries\TMHelper::getAllBloodGroups())
                                        @foreach($bloodGroupList as $bloodGroup )
                                            <option @if(isset($records) && $records->blood_group == $bloodGroup) selected @endif value="{{$bloodGroup}}">{{$bloodGroup}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">बुवाको नाम <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" required name="father_name" value="{{$records->father_name or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">आमाको नाम <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" required name="mother_name" value="{{$records->mother_name or ''}}">
                                </div>
                            </div>
                            <hr>
                            <h2>स्थाई ठेगाना:</h2>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">स्थायी प्रदेश <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <select  required name="permanent_address_state" class="form-control">
                                        <option value="">प्रदेश चयन गर्नुहोस्</option>
                                        @php($stateList = \App\Libraries\TMHelper::getAllStates())
                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                            @foreach($stateList as $state )
                                                <option @if(isset($records) && $records->permanent_address_state == $state) selected @endif value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                        @else
                                            <option  selected value="{{session()->get('locations')->state}}">{{session()->get('locations')->state}}</option>
                                        @endif
                                    </select>
                                </div>
                                    <label class="col-form-label font-weight-bold col-lg-2">जिल्ला <span class="required-astrik">*</span></label>
                                    <div class="col-lg-3">
                                        <select  required name="permanent_address_district" class="form-control">
                                            <option value="">जिल्ला चयन गर्नुहोस्</option>
                                            @php($districtList = \App\Libraries\TMHelper::getAllDistrict())

                                            @if(session()->get('userRoles')->user_roles =='superadmin')
                                                @foreach($districtList as $district )
                                                    <option @if(isset($records) && $records->permanent_address_district == $district) selected @endif value="{{$district}}">{{$district}}</option>
                                                @endforeach
                                            @else
                                                <option  selected  value="{{session()->get('locations')->district}}">{{session()->get('locations')->district}}</option>
                                            @endif
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">गाविस/नगरपालिका/उप-मेट्रो/महानगरीय <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    @if(session()->get('userRoles')->user_roles =='superadmin')
                                        <input type="text" class="form-control" autocomplete="off" required name="permanent_address_municipality" value="{{$records->permanent_address_municipality or ''}}">
                                    @else
                                        <input type="text" class="form-control" autocomplete="off" required readonly name="permanent_address_municipality" value="{{session()->get('locations')->name}}">
                                    @endif
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड नम्बर <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    @if(session()->get('userRoles')->user_roles =='superadmin')
                                    <input type="number" class="form-control" autocomplete="off" required name="permanent_address_ward_no" value="{{$records->permanent_address_ward_no or ''}}">
                                        @else
                                        <input type="number" class="form-control" autocomplete="off" readonly required name="permanent_address_ward_no" value="{{session()->get('users')->ward_no}}">
                                        @endif

                                </div>
                            </div>
                            <hr>
                            <h2>नागरिकता विवरणहरू:</h2>
                                <div class="form-group row">
                                    <label class="col-form-label font-weight-bold col-lg-2">नागरिकताको प्रकार <span class="required-astrik">*</span></label>
                                    <div class="col-lg-3">
                                        <select  required name="citizenship_type" class="form-control">
                                            <option value="">नागरिकताको प्रकार चयन गर्नुहोस्</option>
                                            @php($citizenshipTypeList = \App\Libraries\TMHelper::getAllCitizenshipType())
                                            @foreach($citizenshipTypeList as $type )
                                                <option @if(isset($records) && $records->citizenship_type == $type) selected @endif value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-form-label font-weight-bold col-lg-2">नागरिकता नम्बर <span class="required-astrik">*</span></label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" autocomplete="off" required name="citizenship_number" value="{{$records->citizenship_number or ''}}">
                                    </div>
                                </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">नागरिकता प्रकाशित मिति <span class="required-astrik">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" id="dateissued" class="form-control" autocomplete="off" required name="" >
                                    <input type="hidden" id="dateissuedmain" class="form-control" autocomplete="off" required name="citizenship_issued_date" value="{{$records->citizenship_issued_date or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">नागरिकता अगाडिको तस्वीर</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control" autocomplete="off"  name="citizenship_image_front" value="{{$records->citizenship_image_front or ''}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">नागरिकता पछाडीको तस्वीर</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control" autocomplete="off"  name="citizenship_image_back" value="{{$records->citizenship_image_back or ''}}">
                                </div>
                            </div>
                            @if(isset($records))
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <img src="/uploads/citizenship/{{$records->citizenship_number}}/{{$records->citizenship_image_front}}" width="100px;">
                                </div>
                                <div class="col-lg-3">
                                    <img src="/uploads/citizenship/{{$records->citizenship_number}}/{{$records->citizenship_image_back}}" width="100px;">
                                </div>
                            </div>
                                @endif

                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){



            $("#dateofbirth").nepaliDatePicker({
                dateFormat: "%y-%m-%d",
                closeOnDateSelect: true,
                onChange: function(){
                    alert("The text has been changed.");
                }
            });
            $("#dateissued").nepaliDatePicker({
                dateFormat: "%y-%m-%d",
                closeOnDateSelect: true
            });



            var birthdate = '<?php echo  isset($records->dob) ?  $records->dob : '' ?>';
            var issuedDate = '<?php  echo isset($records->citizenship_issued_date) ?  $records->citizenship_issued_date : '' ?>';


            if(birthdate !=''){
                var birthdate = birthdate.split('-');
                var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", parseInt(birthdate[0]), parseInt(birthdate[1]), parseInt(birthdate[2]));
                $("#dateofbirth").val(formatedNepaliDate);
            }

            if(issuedDate !=''){
                var issuedDate = issuedDate.split('-');
                var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", parseInt(issuedDate[0]), parseInt(issuedDate[1]), parseInt(issuedDate[2]));
                $("#dateissued").val(formatedNepaliDate);
            }


            var currentDate = new Date();
            var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());

            var CurrentNepaliDateInEnglish = currentNepaliDate.bsYear+'-'+currentNepaliDate.bsMonth+'-'+currentNepaliDate.bsDate;


            $("#dateofbirth").on("dateSelect", function (event) {

                year = event.datePickerData.bsYear;
                month = event.datePickerData.bsMonth
                dateday = event.datePickerData.bsDate
               $('#dateofbirthmain').val(year+'-'+('0'+month).slice(-2)+'-'+('0'+dateday).slice(-2));

                var dateFirst = new Date(year+'-'+month+'-'+dateday);
                var dateSecond = new Date(CurrentNepaliDateInEnglish);

                // time difference
                var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());

                // days difference
                var year = Math.ceil(Math.ceil(timeDiff / (1000 * 3600 * 24))/365);

                $('#age').val(year);


            });

            $("#dateissued").on("dateSelect", function (event) {
                year = event.datePickerData.bsYear;
                month = event.datePickerData.bsMonth
                dateday = event.datePickerData.bsDate
                $('#dateissuedmain').val(year+'-'+('0'+month).slice(-2)+'-'+('0'+dateday).slice(-2));

            });

            $('#head_of_family').on('click', function(){


                if($(this).prop('checked')){

                    //$('select[name=pariwar_no]').attr('disabled','disabled');
                    $('select[name=pariwar_no]').css('pointer-events','none');
                    $('select[name=pariwar_no] option[value=""]').attr('selected', 'selected');


                } else {
                    //$('select[name=pariwar_no]').removeAttr('disabled');
                    $('select[name=pariwar_no]').css('pointer-events','all');
                }

            })




        })
    </script>
@endsection