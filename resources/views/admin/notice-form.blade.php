@php($nav = 11)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">सूचना सम्पादन </span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">सूचना सूची</a>
                        <span class="breadcrumb-item active">सूचना सम्पादन</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            @include('flash')
            <div class="card">
                <div class="card-body">
                    <form action="/admin/notice/add" method="post" id="location-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">सूचना सम्पादन</legend>
                            <input type="hidden" class="form-control" name="id" value="{{$notice->id or ''}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सूचना शीर्षक<span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" autocomplete="off" required name="notice_title" value="{{$notice->notice_title or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">वार्ड नम्बर<span class="required-astrik">*</span></label>
                                <div class="col-lg-6">
                                    <select  required name="notice_of_ward_no" class="form-control">

                                        <option value="">वार्ड नम्बर चयन गर्नुहोस्</option>


                                        @if(session()->get('userRoles')->user_roles =='superadmin')
                                            @foreach($wards as $ward)
                                                <option @if(isset($notice->news_of_ward_no) && $notice->news_of_ward_no == $ward->id) selected @endif value="{{$ward->id}}">{{env('SITE_NAME')}} वडा न. {{$ward->ward_no}}</option>
                                            @endforeach
                                        @else
                                            <option  selected  value="{{session()->get('wardTableID')->id}}">{{env('SITE_NAME')}} वडा न. {{session()->get('users')->ward_no}}</option>

                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सूचना विवरण</label>
                                <div class="col-lg-12">
                                    <textarea type="text" class="form-control"  id="editor" autocomplete="off" required name="notice_description" value="{{$notice->notice_description or ''}}">{{$notice->notice_description or ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">सूचना फाइल</label>
                                <div class="col-lg-3">
                                    <input type="file" class="form-control"  id="editor" autocomplete="off"  name="notice_media" value="{{$notice->notice_media or ''}}">
                                </div>
                            </div>
                            @if(isset($notice->notice_media))
                                <div class="form-group row">
                                    <label class="col-form-label font-weight-bold col-lg-2">यहाँबाट सूचना सूचना फाइलहरू डाउनलोड गर्नुहोस्:</label>
                                    <div class="col-lg-3">
                                        <a class="btn btn-success" href="/uploads/notice/notice_files/{{$notice->notice_media}}">डाउनलोड गर्न क्लिक गर्नुहोस्</a>
                                    </div>
                                </div>
                            @endif


                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">पेश गर्नुहोस् <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection