@php($nav = 17)
@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Media Upload</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Add New Media ( IMAGES )</h4>
                    <div class="card">
                        <form action="/admin/media/add" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                        <div class="media-upload-wrapper">
                            <div class="form-wrapper">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <div class="">
                                            <img class="new-img-preview" src="https://via.placeholder.com/200" title="">
                                        </div>
                                            <div class="input-group">
                                            <div class="input-group-btn">
                                            <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                <span><i class="glyphicon glyphicon-upload"></i> Select Image</span>
                                                <input  onchange="readURL(this)" name="media_image[]" type="file" class="attachment_upload">
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="">
                                                <img class="new-img-preview" src="https://via.placeholder.com/200" title="">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> Select Image</span>
                                                        <input  onchange="readURL(this)" name="media_image[]" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="">
                                                <img class="new-img-preview" src="https://via.placeholder.com/200" title="">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> Select Image</span>
                                                        <input  onchange="readURL(this)" name="media_image[]" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ancor-division">
                                    <button type="submit" class="btn btn-success"><span class="pull-right icon-arrow-right16"></span>  SAVE & UPLOAD ALL </button>
                                </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Add New Media ( Video )</h4>
                    <div class="card">
                        <form action="/admin/media/add" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                        <div class="media-upload-wrapper">
                            <div class="form-wrapper">
                                <div class="form-group">
                                    <label for="">Paste video url here ( Youtube )</label>
                                    <input type="text"  name="video_url" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label for="">Title of video</label>
                                    <input type="text" name="video_title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Add Description</label>
                                    <textarea name="video_description" id="" cols="30" rows="10" class="form-control text-box"></textarea>
                                </div>
                            </div>
                            <div class="ancor-division">
                                    <button type="submit" class="btn btn-success"><span class="pull-right icon-arrow-right16"></span>  SAVE & PUBLISH VIDEO </button>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div>


</div>


<script>
    function readURL(thisObj) {
        debugger;
        if (thisObj.files && thisObj.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                debugger;
                $(thisObj).parent().parent().parent().prev().find('img')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
            };
            reader.readAsDataURL(thisObj.files[0]);
        }
    }



</script>
@endsection