@php($nav = 16)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Media Upload</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Edit Media ( Images )</h4>
                    <div class="card">

                        <form action="/admin/media/add" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="media-upload-wrapper">
                            <div class="form-wrapper">

                                <div class="row">
                                    @foreach($images as $media)
                                    <div class="col-md-4">
                                        <input type="hidden" name="ID[]" value="{{$media->id}}">

                                        <div class="form-group">
                                            <div class="">
                                                <img class="new-img-preview" src="/uploads/media/{{$media->media_image}}" title="">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> Replace Image</span>
                                                        <input  onchange="readURL(this)" name="media_image[]" type="file" class="attachment_upload">
                                                    </div>

                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-danger" onclick="return(confirm('Are you sure want to delete this media file?'));" href="/admin/media/{{$media->id}}/delete">Delete Image</a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                            @if(isset($images) && $images->total() > 0)
                                <div class="ancor-division">
                                <button type="submit" class="btn btn-success"><span class="pull-right icon-arrow-right16"></span>  EDIT & UPDATE ALL </button>
                            </div>
                                @endif
                        </div>
            </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Edit Media ( Video )</h4>
                    <div class="card">
                        <div class="media-upload-wrapper">
                            <div class="form-wrapper">
                                <div class="row">
                                    @foreach($videos as $video)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" src="{{$video->video_url}}" allowfullscreen></iframe>
                                            </div>
                                            <a href="#" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#editvideo{{$video->id}}">Edit Video</a>
                                            <a  onclick="return(confirm('Are you sure want to remove this media?'));" href="/admin/media/{{$video->id}}/delete" class="btn btn-lg btn-danger">Remove Video</a>
                                            <br>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>


    </div>


    <script>
        $(document).ready(function() {
            var brand = document.getElementById('new-img-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.new-img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#new-img-id").change(function() {
                readURL(this);
            });
        });
    </script>

    <!-- bootsrap modal for video edit -->
@foreach($videos as $videoModal)
    <div class="modal fade" id="editvideo{{$videoModal->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/admin/media/add" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="ID" value="{{$videoModal->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{$videoModal->video_url}}" allowfullscreen></iframe>
                    </div>
                    <hr>
                    <div class="form-wrapper">
                        <div class="form-group">
                            <label for="">Edit video url here ( Youtube )</label>
                            <input type="text" required name="video_url" value="{{$videoModal->video_url}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Edit Title of video</label>
                            <input type="text" class="form-control" name="video_title" value="{{$videoModal->video_title}}">
                        </div>
                        <div class="form-group">
                            <label for="">Edit Description</label>
                            <textarea name="video_description" value="{{$videoModal->video_description}}" id="" cols="30" rows="6" class="form-control text-box">{{$videoModal->video_description}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            </form>
            </div>
        </div>
    </div>
    @endforeach


    <script>
        function readURL(thisObj) {
            debugger;
            if (thisObj.files && thisObj.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    debugger;
                    $(thisObj).parent().parent().parent().prev().find('img')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(thisObj.files[0]);
            }
        }



    </script>
@endsection