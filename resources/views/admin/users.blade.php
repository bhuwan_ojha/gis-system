@php($nav = 6)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">प्रयोगकर्ता सूची</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">प्रयोगकर्ताहरू</a>
                        <span class="breadcrumb-item active">प्रयोगकर्ता सूची</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            @include('flash')
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>क्रम सङ्ख्या</th>
                    <th>नाम</th>
                    <th>इमेल</th>
                    <th>स्थान</th>
                    <th>वार्ड नम्बर</th>
                    <th>प्रयोगकर्ता भूमिकाहरू</th>
                    <th>स्थिति</th>
                    <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
                </tr>
                </thead>
                <tbody>
                @php($x=1)
                @foreach($users as $user)
                <tr>
                    <td>{{$x++}}</td>
                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                    <td><a href="#">{{$user->email}}</a></td>
                    <td>{{$user->name}}</td>
                    <td>{{env('SITE_NAME')}} वार्ड नम्बर {{$user->ward_no}}</td>
                    <td>{{$user->user_roles}}</td>
                    <td>
                    @if ($user->status == 'Active')
                        <span class="badge badge-success">सक्रिय</span>
                        @elseif($user->status == 'Inactive')
                        <span class="badge badge-danger">निष्क्रिय</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/admin/users/{{$user->id}}/edit-user" class="dropdown-item"><i class="icon-pencil"></i> सम्पादन गर्नुहोस्</a>
                                    <a href="/admin/users/{{$user->id}}/delete" onclick="return(confirm('के तपाईं पक्का यो प्रयोगकर्ता मेटाउन चाहनुहुन्छ? ?'))" class="dropdown-item"><i class="icon-trash"></i> मेटाउनुहोस्</a>
                                    <a href="/admin/users/{{$user->id}}/active" onclick="return(confirm('के तपाई निश्चित हुनुहुन्छ यो प्रयोगकर्ता सक्रिय गर्न चाहानुहुन्छ ?'))" class="dropdown-item"><i class="icon-check"></i> सक्रिय गर्नुहोस्</a>
                                    <a href="/admin/users/{{$user->id}}/inactive" onclick="return(confirm('के तपाई निश्चित हुनुहुन्छ यो प्रयोगकर्ता निष्क्रिय गर्न चाहानुहुन्छ ?'))" class="dropdown-item"><i class="icon-cross"></i> निष्क्रिय गर्नुहोस्</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
            <div class="col-md-12 offset-6">{{ $users->links() }}</div>
        </div>
@endsection
































