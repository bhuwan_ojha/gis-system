@php($nav = 2)
@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">रेकर्ड सूची</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">रेकर्ड</a>
                        <span class="breadcrumb-item active">रेकर्ड सूची</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            @include('flash')
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>क्रम सङ्ख्या</th>
                    <th>नागरिकता नम्बर</th>
                    <th>नाम</th>
                    <th>लिङ्ग</th>
                    <th>प्रदेश</th>
                    <th>जिल्ला</th>
                    <th>वार्ड नम्बर</th>
                                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
                </tr>
                </thead>
                <tbody>
                @php($x=1)
                @foreach($records as $record)
                <tr>
                    <td>{{$x++}}</td>
                    <td>{{$record->citizenship_number}}</td>
                    <td><a href="#">{{$record->full_name}}</a></td>
                    <td>{{$record->gender}}</td>
                    <td>{{$record->permanent_address_state}}</td>
                    <td>{{$record->permanent_address_district}}</td>
                    <td>{{$record->permanent_address_ward_no}}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/admin/records/{{$record->id}}/edit-record" class="dropdown-item"><i class="icon-pencil"></i> सम्पादन गर्नुहोस्</a>
                                    <a href="/admin/records/{{$record->id}}/delete" onclick="return(confirm('के तपाई निश्चित हुनुहुन्छ कि यो रेकर्ड मेटाउन चाहानुहुन्छ ?'))" class="dropdown-item"><i class="icon-trash"></i> मेटाउनुहोस्</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
            <div class="col-md-12 offset-6">{{ $records->links() }}</div>
        </div>
@endsection