<!DOCTYPE html>
<html lang="en">



<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>{{env('SITE_NAME')}} - Admin Panel</title>


	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">



	<script src="/admin-assets/js/main/jquery.min.js"></script>
	<script src="/admin-assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="/admin-assets/js/plugins/loaders/blockui.min.js"></script>



	<script src="/admin-assets/js/app.js"></script>


</head>

<body>


	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="/" class="d-inline-block">
				<img src="/assets/images/logo/logo.png" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>
	</div>




	<div class="page-content">


		<div class="content-wrapper">
			<div class="content d-flex justify-content-center align-items-center">
				<form class="login-form" action="/admin/login-submit" method="post">
					{{csrf_field()}}
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<img class="login-logo" src="/assets/images/logo/logo.png" alt="">
								<h5 class="mb-0">{{env('SITE_NAME')}}</h5>
								<span class="d-block text-muted">Login to your account</span>
							</div>
							@include('flash')
							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" name="email" class="form-control" placeholder="Email Address" autocomplete="off">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

							<div class="text-center">
								<a href="/password/reset">Forgot password?</a>
							</div>
						</div>
					</div>
				</form>


			</div>
		</div>


	</div>


</body>


</html>
