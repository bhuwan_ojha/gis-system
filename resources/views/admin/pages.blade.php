@php($nav = 420)
@extends('admin.layouts.layout')
@section('content')


    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">सूचना सूची</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> गृहपृष्ट</a>
                        <a href="#" class="breadcrumb-item">सूचना</a>
                        <span class="breadcrumb-item active">सूचना सूची</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            @include('flash')
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>क्रम सङ्ख्या</th>
                    <th>स्लुग</th>
                    <th>सूचना शीर्षक</th>
                    <th>सिर्जना मिति</th>

                                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
                </tr>
                </thead>
                <tbody>
                @php($x=1)
                @foreach($pagesList as $page)
               <tr>
                    <td>{{$x++}}</td>
                    <td>{{$page->page_title}}</td>
                    <td>{{$page->page_slug}}</td>
                    <td>{{$page->created_at}}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/admin/Pages/{{$page->id}}/edit-pages" class="dropdown-item"><i class="icon-pencil"></i> सम्पादन गर्नुहोस्</a>
                                    <a href="/admin/Pages/{{$page->id}}/delete" onclick="return(confirm('Are you sure want to delete this page ?'))" class="dropdown-item"><i class="icon-trash"></i> मेटाउनुहोस्</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
            <div class="col-md-12 offset-6">{{ $pagesList->links() }}</div>
        </div>
@endsection