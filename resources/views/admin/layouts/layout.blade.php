<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>{{env('SITE_NAME')}} - व्यवस्थापक प्यानल</title>



    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/admin-assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/select2.min.css" rel="stylesheet" type="text/css">


    <script src="/admin-assets/js/main/jquery.min.js"></script>
    <script src="/admin-assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/admin-assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="/admin-assets/js/plugins/editors/ckeditor/ckeditor.js"></script>


    <script src="/admin-assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/admin-assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/admin-assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/admin-assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/admin-assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/admin-assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/admin-assets/js/select2.full.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

    <script src="/admin-assets/js/app.js"></script>
    <script src="/admin-assets/js/demo_pages/dashboard.js"></script>
{{--    <link href="/admin-assets/js/summernote/summernote.css" rel="stylesheet">
    <script src="/admin-assets/js/summernote/summernote.js"></script>--}}
    <script src="/admin-assets/js/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="/admin-assets/css/nepaliDatePicker.min.css">
    <script type="text/javascript" src="/admin-assets/js/jquery.nepaliDatePicker.js"></script>
    <script type="text/javascript" src="/admin-assets/js/demo.js"></script>

    <style>
        .ui-datepicker .ui-datepicker-header {
            background: #1D90C5;
        }
        .ui-datepicker-days-cell-over {
            background: #1D90C5;
        }
        .ui-state-active { color: black !important;}
        .ui-datepicker-prev { top:50% !important;}
        .ui-datepicker-next { top:50% !important;}
    </style>

</head>

<body>
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="/admin/dashboard" class="d-inline-block">
            <img src="/assets/images/logo/logo.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="badge bg-success ml-md-3 mr-md-auto">अनलाइन</span>

        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="/admin-assets/images/demo/users/face11.jpg" class="rounded-circle mr-2"
                         height="34" alt="">
                    <span>प्रशासक</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/admin/users/profile" class="dropdown-item"><i class="icon-user-plus"></i> मेरो प्रोफाइल</a>
                    <div class="dropdown-divider"></div>
                    <a href="/admin/logout" class="dropdown-item"><i class="icon-switch2"></i> बाहिर निस्कनु</a>
                </div>
            </li>
        </ul>
    </div>
</div>


<div class="page-content">
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">


        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>


        <div class="sidebar-content">


            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="/admin-assets/images/demo/users/face11.jpg" width="38"
                                             height="38" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold">{{\Illuminate\Support\Facades\Auth::user()->first_name}} {{\Illuminate\Support\Facades\Auth::user()->last_name}}</div>
                            <div class="font-size-xs opacity-50">
                                <i class="icon-pin font-size-sm"></i> {{env('SITE_ADDRESS')}}
                            </div>
                        </div>

                        <div class="ml-3 align-self-center">
                            <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">


                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">मुख्य</div>
                        <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="/admin/dashboard" class="nav-link @if($nav == 1)active @endif">
                            <i class="icon-home4"></i>
                            <span>
									ड्यासबोर्ड
								</span>
                        </a>
                    </li>
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">
                            सामग्री व्यवस्थापक</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 8 || $nav == 9 || $nav == 10 || $nav == 11 || $nav == 16 || $nav == 17) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav ==8 || $nav == 9 || $nav == 10 || $nav == 11 || $nav == 16 || $nav == 17)active @endif"><i class="icon-page-break2"></i> <span>सामग्री व्यवस्थापक</span></a>
                        <ul class="nav nav-group-sub" @if($nav ==8 || $nav == 9 || $nav == 10 || $nav == 11 || $nav == 16 || $nav == 17) style="display: block!important;" @endif>
                            <li class="nav-item"><a href="/admin/news"  class="nav-link @if($nav == 8)active @endif">
                                    समाचार व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/news/add-news" class="nav-link @if($nav == 9)active @endif">
                                    नयाँ समाचारहरू थप्नुहोस्</a></li>

                            <li class="nav-item"><a href="/admin/notice"  class="nav-link @if($nav == 10)active @endif">सूचना प्रबन्धक</a></li>
                            <li class="nav-item"><a href="/admin/notice/add-notice" class="nav-link @if($nav == 11)active @endif">नयाँ सूचना थप गर्नुहोस्</a></li>
                            @if(session()->get('userRoles')->user_roles == 'superadmin' || session()->get('userRoles')->user_roles == 'admin')
                            <li class="nav-item"><a href="/admin/media"  class="nav-link @if($nav == 16)active @endif">मिडिया प्रबन्धक</a></li>
                            <li class="nav-item"><a href="/admin/media/add-media" class="nav-link @if($nav == 17)active @endif">नयाँ मिडिया थप गर्नुहोस्</a></li>
                                @endif
                        </ul>
                    </li>
                    @if(session()->get('userRoles')->user_roles == 'superadmin' || session()->get('userRoles')->user_roles == 'admin')
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">रेकर्ड व्यवस्थापन</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 2 || $nav == 3) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 2 || $nav == 3)active @endif"><i class="icon-page-break2"></i> <span>रेकर्ड प्रबन्धक</span></a>
                        <ul class="nav nav-group-sub" @if($nav == 2 || $nav == 3) style="display: block!important;" @endif>
                            <li class="nav-item"><a href="/admin/records"  class="nav-link @if($nav == 2)active @endif">डेटा प्रबन्धक</a></li>
                            <li class="nav-item"><a href="/admin/records/add-record" class="nav-link @if($nav == 3)active @endif">नयाँ रेकर्ड थप गर्नुहोस्</a></li>
                        </ul>
                    </li>
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">सम्पत्ति व्यवस्थापन</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 14 || $nav == 15 || $nav==12 || $nav==13) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 14 || $nav == 15 || $nav==12 || $nav==13)active @endif"><i class="icon-page-break2"></i> <span>सम्पत्ति व्यवस्थापक</span></a>
                        <ul class="nav nav-group-sub" @if($nav == 14 || $nav == 15 || $nav==12 || $nav==13) style="display: block!important;" @endif>
                            <li class="nav-item"><a href="/admin/property"  class="nav-link @if($nav == 14)active @endif">सम्पत्ति व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/property/add-property" class="nav-link @if($nav == 15)active @endif">नयाँ सम्पत्ति रेकर्ड थप्नुहोस्</a></li>
                            <li class="nav-item"><a href="/admin/public-property"  class="nav-link @if($nav == 12)active @endif">सार्वजनिक सम्पत्ति व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/public-property/add-property" class="nav-link @if($nav == 13)active @endif">नयाँ सार्वजनिक सम्पत्ति रेकर्ड थप्नुहोस्</a></li>
                        </ul>
                    </li>
                    @endif
                   @if(session()->get('userRoles')->user_roles == 'superadmin')

                        <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">स्थान व्यवस्थापन</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 4 || $nav == 5) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 4 || $nav == 5)active @endif"><i class="icon-location3"></i> <span>स्थान व्यवस्थापक</span></a>
                        <ul class="nav nav-group-sub" @if($nav == 4|| $nav == 5) style="display: block!important;" @endif>
                            <li class="nav-item"><a href="/admin/locations" class="nav-link @if($nav == 4)active @endif">स्थान व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/locations/add-location" class="nav-link @if($nav == 5)active @endif">नयाँ स्थान थप गर्नुहोस्</a></li>

                        </ul>
                    </li>
                   @endif
                    @if(session()->get('userRoles')->user_roles =='superadmin' OR session()->get('userRoles')->user_roles =='admin' )
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">प्रयोगकर्ता व्यवस्थापन</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 6|| $nav == 7) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 6 || $nav == 7)active @endif"><i class="icon-user"></i> <span>प्रयोगकर्ता व्यवस्थापक</span></a>
                        <ul class="nav nav-group-sub"  @if($nav == 6|| $nav == 7) style="display: block!important;" @endif>
                            <li class="nav-item "><a href="/admin/users" class="nav-link @if($nav == 6)active @endif">प्रयोगकर्ता व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/users/add-user" class="nav-link @if($nav == 7)active @endif">नयाँ प्रयोगकर्ता थप गर्नुहोस्</a></li>
                        </ul>
                    </li>
                        @endif

                    @if(session()->get('userRoles')->user_roles == 'superadmin' || session()->get('userRoles')->user_roles == 'admin')
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">हाम्रो परिचय</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 920 || $nav == 921) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 920 || $nav == 921)active @endif"><i class="icon-page-break2"></i> <span>हाम्रो परिचय</span></a>
                        <ul class="nav nav-group-sub" @if($nav == 920 || $nav == 921) style="display: block!important;" @endif>
                            <li class="nav-item"><a href="/admin/about/"  class="nav-link @if($nav == 920)active @endif">हाम्रो परिचय</a></li>
                            <li class="nav-item"><a href="/admin/about-form" class="nav-link @if($nav == 921)active @endif">हाम्रो परिचय व्यवस्थापन गर्नुहोस्</a></li>
                        </ul>
                    </li>
                    @endif

                    @if(session()->get('userRoles')->user_roles == 'superadmin' || session()->get('userRoles')->user_roles == 'admin')
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">पृष्ठ व्यवस्थापक</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu @if($nav == 786|| $nav == 420) nav-item-open @endif">
                        <a href="#" class="nav-link @if($nav == 420|| $nav == 786)active @endif"><i class="icon-user"></i> <span>पृष्ठ व्यवस्थापक</span></a>
                        <ul class="nav nav-group-sub"  @if($nav == 420|| $nav == 786) style="display: block!important;" @endif>
                            <li class="nav-item "><a href="/admin/Pages" class="nav-link @if($nav == 420)active @endif">पृष्ठ व्यवस्थापक</a></li>
                            <li class="nav-item"><a href="/admin/Pages/add-pages" class="nav-link @if($nav == 786)active @endif">नयाँ पृष्ठ थप गर्नुहोस्</a></li>
                        </ul>
                    </li>
                        @endif
                </ul>
            </div>


        </div>


    </div>



    @yield('content')


</div>

<script src="/assets/mapdata/darchula.geojson"></script>


<style>
    #backendMap { width: 100%;height: 100%}
</style>
<script>


    function initMap() {
        var locationcordinate = {};

        var myLatLng = {lat: 29.811389, lng: 80.872500};


        var map = new google.maps.Map(document.getElementById('backendMap'), {
            zoom: 10,
            center: myLatLng,
            mapTypeId: 'hybrid',
            labels:true
        });

        map.data.addGeoJson(darchula);

        map.data.setStyle({
            fillColor: 'yellow',
            strokeWeight: 1
        });

        var infowindow =  new google.maps.InfoWindow({});


        var CreateMarker = function(kittanumber,coordinates,fullname,mapObject){

            // var kittanumber = kittanumber+fullname.replace(" ", "");
            var kittanumber = kittanumber;
            var cordinateData = coordinates;
            var name = fullname+'  ('+kittanumber+')';
            var namevarialbe = fullname.replace(" ", "");

            var Lat = parseFloat(cordinateData.split(',')[0]);
            var Lng = parseFloat(cordinateData.split(',')[1]);
            //marker.setPosition( new google.maps.LatLng(29.411389, 80.672500 ) );
            mapObject.panTo( new google.maps.LatLng(Lat, Lng ) );

            locationcordinate[kittanumber] = new google.maps.Marker({
                position: {lat:Lat, lng:Lng},
                map: mapObject,
                title: name
            });

            locationcordinate[kittanumber].addListener('click', function() {
                infowindow.setContent( locationcordinate[kittanumber].getTitle());
                infowindow.open(mapObject,  locationcordinate[kittanumber]);

            });

        }


        var RemoveMarker = function (){

            $.each(locationcordinate,function(key, value){
                locationcordinate[key].setMap(null);

            })

        }

        CreateMarker('526','29.811389,80.872500','Gau Palika Office',map);


        $(document).on('click','.kittanumberdata',function(){
            // debugger;
            RemoveMarker()
            $('#mapsearch').val($(this).text());
            $(this).parents('form').hide();

            CreateMarker($(this).attr('kittanumber'),$(this).attr('value'),$(this).attr('fullname'),map);

        })



        $(document).on('change','.categoryListHome',function(){


            RemoveMarker()

           // $('.childcheckboxlist:checkbox:checked').each(function(){

            if($(this).val() =='All') {


                $.each(Categortdatalist,function(obj){

                    $.each(Categortdatalist[obj], function (key, value) {

                        CreateMarker(value.kitta_number, value.coordinate, value.full_name, map);

                    })

                })

                
            } else {

                $.each(Categortdatalist[$(this).val()], function (key, value) {

                    CreateMarker(value.kitta_number, value.coordinate, value.full_name, map);


                })

            }

        })

        $(document).on('click','.kittanumberdataclass',function(){
            $('#mapsearch').val($(this).text());
            $('.searchresult').hide();
            RemoveMarker()
            CreateMarker($(this).attr('kittanumber'),$(this).attr('value'),$(this).attr('fullname'),map);

        })


    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3-6IY2YKaXOCiP0chUXekibYikJfCsyQ&callback=initMap" async defer></script>


    <script>
        $(function () {
            setNavigation();
        });

        function checkPropertyCategory(thisObj) {
            if ($(thisObj).val() !=='Private Property'){

            }
        }


        function setNavigation() {
            var path = window.location.pathname;
            path = path.replace(/\/$/, "");
            path = decodeURIComponent(path);

            $(".nav a").each(function () {
                var href = $(this).attr('href');
                if (path.substring(0, href.length) === href) {
                    $(this).closest('li').addClass('active');
                }
            });
        }


        function removeItem(thisObj) {
            $(thisObj).closest('div').remove();
        }

        function addMoreWards(thisObj) {
        $('.wards').append('<div class="ward-add">' +
                '<div class="form-group row">'+
                '<label class="col-form-label font-weight-bold col-lg-2">वार्ड विवरणहरू <span class="required-astrik">*</span></label>'+
                '<div class="col-lg-3">'+
                '<input type="number" required min="1" class="form-control" placeholder="वार्ड नम्बर" autocomplete="off" name="ward_no[]" value="1">'+
                '</div>'+
                '<div class="col-lg-3">'+
                '<input type="text" required class="form-control" placeholder="वार्ड ठेगाना" autocomplete="off" name="ward_address[]" value="">'+
                '</div>'+
                '</div>'+
                '<div class="row">'+
                '<label class="col-form-label font-weight-bold col-lg-2">वार्ड कार्यालय फोटो</label>'+
                '<div class="col-lg-6">'+
                '<input type="file" required class="form-control" autocomplete="off" name="ward_image[]" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-8 delete-wards text-right"><button type="button" class="btn btn-danger" onclick="removeThisWards(this)">मेटाउनुहोस् <i class="icon-trash ml-2"></i></button></div>'+
            '</div>');
        }

        function removeWards(thisObj) {
            $(thisObj).parent().parent().remove();
        }

    </script>
    <script>
        $(document).ready(function() {
            CKEDITOR.env.isCompatible = true;
            CKEDITOR.replace( 'editor' );

        });
            $.validator.methods.coordinate = function( value, element ) {

                   // var cordinate = "40.44,-74.044";

                    return /^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/.test(value)

                /*if(value.indexOf(",")) {
                    var reg = new RegExp("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/");
                     cordinatedata = value.split(',');
                     debugger;
                    if( reg.test(cordinatedata[0]) && reg.test(cordinatedata[1])) {

                        return true;
                    }
                    //return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);


                }
                return false */
            }
        $("form").validate({
            rules:{

                coordinate:{
                    coordinate: true
                }
            },
            messages: {
                coordinate: {

                    coordinate: jQuery.validator.format("INVALID FORMAT : Lat & Long sperated by comma & will only have number")
                }
            }
        });



        </script>
//twak code

</div>
</body>
</html>

