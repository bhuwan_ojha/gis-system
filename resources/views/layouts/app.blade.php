<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{env('SITE_NAME')}} - Admin Panel</title>


    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">



    <script src="/admin-assets/js/main/jquery.min.js"></script>
    <script src="/admin-assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/admin-assets/js/plugins/loaders/blockui.min.js"></script>



    <script src="/admin-assets/js/app.js"></script>


</head>

<body>


<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="/" class="d-inline-block">
            <img src="/assets/images/logo/logo.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
    </div>
</div>




<div class="page-content">

@yield('content')

</div>


</div>


</body>


</html>
