@extends('frontend.layouts.layout')
@section('content')
    <?php //dd($message); ?>
<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contact-left">
                <h2>सम्पर्क</h2>
                <div class="contact-content">
                <span class="contact-title" >नेपाल सरकार</span><br>
                <span><strong>अपी गाउपालिका,दार्चुला </strong></span><br>
                <span >महाकाली , सुदुरपश्चिमप्रदेश,नेपाल</span><br>
                </div>
                <div class="detail-tilte">
                    <h3 >जीआईएस प्रणाली</h3>
                </div>
                <div class="detail-content" >
                    <span >सम्पर्क </span><br>
                    <span >फोन : <a href="tel:+977-1-123456">(+977-1)123456</a></span><br>
                    <span >वेबसाईट : <a href="www.apihimalmun.gov.np">www.apihimalmun.gov.np</a></span><br>
                    <span >ईमेल : <a href="mailto:apihimaldarchula@gmail.com"> apihimaldarchula@gmail.com</a></span><br>
                </div>
                </div>
            </div>
            <div class="col-sm-6">
            <div class="contact-wrapper">
                <div class="contact-right"><!-- 
                <h2>Have Any Query! Send Us Message</h2> -->
            <div class="form-wrapper">
                <form action="/sendMail" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">पहिलो नाम</label>
                                <input type="text" name="first_name" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">थर</label>
                                <input type="text" name="last_name" required class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">इमेल </label>
                        <input type="email" name="email" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">सन्देश </label>
                        <textarea name="message" id="" cols="30" rows="5"  class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary-gis"><span class="pull-right ion-chevron-right"></span> पेश गर्नुहोस्</button>
                        <?php echo $altermessage; ?>
                    </div>
                </form>
            </div> 
            </div>
        </div>
            </div>
        </div>
        
    </div>
</section>  
<section class="map-contact-section">
                    <div class="embed-responsive embed-responsive-100x400px">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5481.3060535184!2d80.959421!3d29.810083000000002!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a3f84948351961%3A0x4d26455eaa3986e5!2sKhandeswori+10100!5e1!3m2!1sen!2snp!4v1550904815042" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
</section>

@endsection

        