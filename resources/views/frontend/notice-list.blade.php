@extends('frontend.layouts.layout')
@section('content')
    <section class="notice-wrapper">
    <div class="container">
        <div class="heading-text">
            <h1>सूचनाहरू</h1>
        </div>
    </div>
</section>  
<section class="notice-list-wrapper">
<div class="container">
    <div class="news-praper">
         @foreach($noticeList as $notice)
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-sm-4">
                    <figure>

                    <div class="notice-img">
                        @if(!empty($notice->files))
                        <img src="/uploads/notice/{{$notice->files}}" alt="notice-file" class="img-responsive">
                            @else
                            <img src="/assets/image/nepal-gov.png" alt="notice-file" class="img-responsive">
                        @endif

                    </div>
                    </figure>
                </div>
                <div class="col-sm-8">
                    <div class="news-point">
                        <h4>{{$notice->notice_title}}</h4>
                        <p>{{substr(strip_tags($notice->notice_description),0,102)}}...</p>
                        <ul class="list-unstyled">
                            <li>प्रकाशित मिति <i> {{date_format($notice->created_at,"l jS F Y")}}</i><span class="pull-right"><a href="/notice/{{$notice->notice_of_ward_no}}/{{$notice->id}}/notice-detail">थप पढ्नुहोस्</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
         @endforeach
    </div>
    <div class="pagination-wrapper text-center">{{$noticeList->links()}}</div>
</div>
</section>
@endsection