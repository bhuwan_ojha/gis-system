@extends('frontend.layouts.layout')
@section('content')
   
    <section class="notice-wrapper notice-details">
    <div class="container">
        <div class="heading-text">
            <h2>{{$notice->notice_title }}</h2>
            <ul class="list-inline">
                <li>प्रकाशित मिति <i> {{date_format($notice->created_at,"l jS F Y")}}</i></li>
                <li class="pull-right"><a href="{{'/uploads/notice/notice_files/'.$notice->notice_media}}" class="btn btn-outline-white"> सूचना फाइल डाउनलोड गर्नुहोस्  </a></li>
            </ul>
        </div>
    </div>
</section>

    <div class="notice-section">
        <div class="container">
            <div class="notice-data-blog">
                <a href="/notice" class="back"><span class="ion-android-arrow-up-left"></span> सूचनामा फर्कनुहोस् </a>
                <div class="notice-main-img">
                    <a href="{{'/uploads/notice/'.$notice->files}}" data-lightbox="roadtrip"><img src="{{'/uploads/notice/'.$notice->files}}" alt="" class="img-responsive"></a>
                </div>
                <div class="notice-data">
                    <p> {{strip_tags($notice->notice_description)}}...</p>

                    <div class="media-phase">
                        @if($moreNotice->total() > 0)
                            <h3>थप समाचार</h3>
                            <div class="row">
                                @foreach($moreNotice as $notice)
                                    <div class="col-md-3">
                                        <a href="{{'/uploads/notice/'.$notice->files}}" data-lightbox="roadtrip"><img src="{{'/uploads/notice/'.$notice->files}}" alt="notice-image" class="img-responsive"></a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection