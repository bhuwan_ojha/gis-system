@extends('frontend.layouts.layout')
@section('content')
    <section class="gallery-bg">
        <div class="container">
            <h1>मिडिया र गैलरी</h1>
            <p></p>

        </div>
    </section>
    <section class="gallery-body">
        <div class="container">
            <div class="tab-wrapper">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">तस्बिरहरू</a></li>
                    <li><a data-toggle="tab" href="#menu1">भिडियोहरू</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <!-- <div class="row no-gutters">
                            @if(!empty($images))
                        @foreach($images as $key=>$image)
                                <div class="@if($key==0) col-md-8 @else  col-md-4 @endif">
                                <a href="{{'/uploads/media/'.$image->media_image}}" data-lightbox="roadtrip"><img src="{{'/uploads/media/'.$image->media_image}}" alt="" class="img-responsive"></a>
                            </div>
                            @endforeach
                        @endif
                                </div> -->
                        <section id="thumbsList">
                            @if(!empty($images))
                                @foreach($images as $key=>$image)
                                    <ul class="@if($key==0) col-md-7 @else  col-md-5 @endif">

                                        <li>
                                            <img src="{{'/uploads/media/'.$image->media_image}}" alt="" class="img-responsive"/>
                                        </li>
                                    </ul>
                                @endforeach
                            @endif
                        </section>
                        <div id="lightBox">
                            <img id="lightBoxImage" src="" alt="" />
                            <div id="prevImg"><span></span></div>
                            <div id="nextImg"><span></span></div>
                            <div id="closeLightbox-button"></div>
                        </div>




                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div class="row no-gutters">
                            @if(!empty($videos))
                                @foreach($videos as $key=>$video)
                                    <div class="col-md-6">
                                        <iframe width="420" height="225" src="{{$video->video_url}}"></iframe>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection