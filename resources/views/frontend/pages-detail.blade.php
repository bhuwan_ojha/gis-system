@extends('frontend.layouts.layout')
@section('content')


    <?php
    $pagesData= json_decode($pagesData[0]);

         ?>
    <section class="notice-wrapper notice-details">
    <div class="container">
        <div class="heading-text">
            <h2>{{$pagesData->page_title }}</h2>
            <ul class="list-inline">


            </ul>
        </div>
    </div>
</section>

    <div class="notice-section">
        <div class="container" style="width: 100%!important;">
            <div class="">
                {{--<div class="notice-main-img">
                    <a href="{{'/uploads/pages/pages_files'.$pagesData->files}}" data-lightbox="roadtrip"><img src="{{'/uploads/pages/pages_files'.$pagesData->files}}" alt="" class="img-responsive"></a>
                </div>--}}
                <div class="">
                     {!! $pagesData->page_description !!}


                </div>
            </div>
        </div>
    </div>


@endsection