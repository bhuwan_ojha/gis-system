@extends('frontend.layouts.layout')
@section('content')
<section class="policies-wrapper">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-md-3 fixed">
                <div class="policies-pills-wrapper">
                    <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#home" data-toggle="tab" >Home</a></li>
                    <li><a href="#menu1" data-toggle="tab">Menu 1</a></li>
                    <li><a href="#menu2" data-toggle="tab" >Menu 2</a></li>
                    <li><a href="#menu3" data-toggle="tab">Menu 3</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                 <div class="pills-content">
                 <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <h4>Home</h4>
                        <div class="pills-content-wrapper">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h4>Menu 1</h4>
                        <div class="pills-content-wrapper">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h4>Menu 2</h4>
                        <<div class="pills-content-wrapper">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod modi sequi voluptas mollitia ducimus soluta facilis reiciendis odit, quae tenetur! Commodi asperiores doloribus eveniet voluptas quis accusamus consectetur non libero!</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos quod impedit dicta quo asperiores fuga ad. Sunt quibusdam magni, suscipit soluta ad, laudantium aut saepe voluptates sit officiis laborum! Quidem.</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur minus vel quo, optio blanditiis odio cumque? Maiores, obcaecati quae voluptatum numquam ratione incidunt est perferendis, dolor quis rerum quam nihil.</p>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem, veniam quas et numquam perspiciatis, voluptas est magni quasi a qui laboriosam illo sunt ipsam fuga quo, nesciunt quis illum aliquam.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore consectetur fugiat harum eum praesentium, delectus doloribus. Corrupti aspernatur ex nisi temporibus illo illum doloremque, molestias vel architecto laudantium odit blanditiis?</p>
                        </div>
                    </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section>

@endsection