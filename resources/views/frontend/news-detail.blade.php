@extends('frontend.layouts.layout')
@section('content')
   
    <section class="notice-wrapper notice-details">
    <div class="container">
        <div class="heading-text">
            <h2>{{$news->news_title}}</h2>
            <ul class="list-inline">
                <li>प्रकाशित मिति <i> {{date_format($news->created_at,"l jS F Y")}}</i></li>
            </ul>
        </div>
    </div>
</section>  

<div class="notice-section">
    <div class="container">
    <div class="notice-data-blog">
        <a href="/news" class="back"><span class="ion-android-arrow-up-left"></span> समाचारमा फर्कनुहोस् </a>
        <div class="news-main-img">
        <a href="{{'/uploads/news/'.$news->files}}" data-lightbox="roadtrip"><img src="{{'/uploads/news/'.$news->files}}" alt="" class="img-responsive"></a>
        </div>
        <div class="notice-data">
            <p> {{strip_tags($news->news_description)}}...</p>

            <div class="media-phase">
            @if($moreNews->total() > 0)
                <h3>थप समाचार</h3>
            <div class="row">
                        @foreach($moreNews as $news)
                        <div class="col-md-3">
                        <a href="{{'/uploads/news/'.$news->files}}" data-lightbox="roadtrip"><img src="{{'/uploads/news/'.$news->files}}" alt="news-image" class="img-responsive"></a>
                        </div>
                @endforeach
            </div>
                @endif
            </div>

            
        </div>
    </div>
    </div>
</div>


@endsection