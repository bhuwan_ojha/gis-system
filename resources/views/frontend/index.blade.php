@extends('frontend.layouts.layout')
@section('content')
    <section class="heading-text">
        <div class="container">
            <div class="section-wrapper-padding">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="banner-left">
                            <h1>
                                जीआईएस <span>प्रणाली</span>
                            </h1>
                            <p>
                                भौगोलिक सूचना प्रणाली (जीआईएस) एक प्रणाली हो जुन स्थानिक वा भौगोलिक डेटा खिच्ने, भण्डारण, विश्लेषण, व्यवस्थापन गर्ने, र पेश गर्न डिजाइन गरिएको प्रणाली हो।
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 text-right">
                        <div class="banner-right">
                            <img src="{{'/assets/image/nepal-gov.png'}}" alt="ward-image" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="map">
        <div class="embed-responsive embed-responsive-100x400px" id="mymap">
            <!--<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3532.1855782835787!2d85.32911900000151!3d27.711555900013337!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1548994733904" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> -->
        </div>
    </section>

    <section class="main-element-gis">
        <div class="container">

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form action="" class="gis-form-wrapper">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <select class="form-control categoryListHome" id="sel1">
                                        <option value="All">सबै <span class="caret"></span> </option>

                                        @foreach($category as $key=>$location)
                                            @if($key =='Private Property' && isset(session()->get('userRoles')->user_roles) && session()->get('userRoles')->user_roles =='manager' )
                                                @continue
                                            @endif

                                                @if($key =='Private Property' && !isset(session()->get('userRoles')->user_roles)  )
                                                    @continue
                                                @endif
                                            <option value="{{ $key }}">{{ $location }}</option>
                                        @endforeach


                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group" id="sell2">
                                    <input id="mapsearch" type="text" class="form-control" placeholder="यहाँ खोज्नुहोस्">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 searchresult" style="background: white;margin-top: 14px;margin-left: 272px;display: none;position: absolute;z-index: 2">
                            <div class="form-group"  >
                                <ul  style="list-style-type: none;padding-left: 0px;" id="searchfieldlist1">

                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="block-component-wrapper">
                <div class="row">
                    @foreach($allWardList as $wards)
                        <div class="col-sm-4">
                            <div class="block-component">
                                <ul class="list-inline">
                                    @if(!empty($wards->ward_image))
                                        <li><span><img src="{{'/uploads/ward/'.$wards->ward_image}}" alt="ward-image"></span></li>
                                    @else
                                        <li><span><img src="{{'/assets/image/nepal-gov.png'}}" alt="ward-image"></span></li>
                                    @endif
                                </ul>
                                <h4>
                                    {{env('SITE_NAME').' '. 'वार्ड नम्बर:'.' '.$wards->ward_no}}
                                </h4>
                                <p><i class="fa fa-map-marker"></i> वार्ड कार्यालय ठेगाना: {{ $wards->ward_address}}</p>
                                <div class="row buttons">
                                    <div class="col-xs-6">
                                        <a href="/news/{{$wards->id}}/news-list" class="newsbtn">समाचार</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="/notice/{{$wards->id}}/notice-list" class="noticebtn">सूचना</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    @endforeach
                </div>


            </div>
        </div>
    </section>

    <section class="about-us">
        <div class="about-us-wrapper">
            <div class="row">
                <div class="col-md-8">
                    <div class="about-us-text-wrapper">
                        <div class="container">
                            <h1>
                                <span> हाम्रो </span> परिचय
                            </h1>
                            <p>
                                नेपालको प्रदेश नं. ७ को दार्चुला जिल्लामा अवस्थित यस अपिहिमाल गाउ“पालिका भौगोलिक हिसाबले करिब ८०ं५०’ देखि ८१ं६’ पुर्वी देशान्तर र २९ं५०’ देखि ३०ं८०’ उत्तर अंक्षाशमा रहेको छ । करिब ६१३.९५ व.कि.मी. क्षेत्रफल रहेको गाउ“पालिकाको पुर्वमा बझाङ जिल्ला, पश्चिमतिर नौगाड गाउ“पालिका, दक्षिणमा मार्मा गाउ“पालिका र उत्तरमा व्यास गाउ“पालिका रहेका छन । अपिहिमाल गाउ“पालिका मिति २०७३ फागुन...
                            </p>
                            <a target="_blank" href="http://apihimalmun.gov.np/content/%E0%A4%85%E0%A4%AA%E0%A4%BF%E0%A4%B9%E0%A4%BF%E0%A4%AE%E0%A4%BE%E0%A4%B2-%E0%A4%97%E0%A4%BE%E0%A4%89%E0%A4%AA%E0%A4%BE%E0%A4%B2%E0%A4%BF%E0%A4%95%E0%A4%BE%E0%A4%95%E0%A5%8B-%E0%A4%AA%E0%A4%B0%E0%A4%BF%E0%A4%9A%E0%A4%AF">अधिक जान्नुहोस्</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-slider">

                        @foreach( $aboutUs as $about)

                            <div class="image">
                                <img src="/uploads/about/{{ $about->media_image }}" alt="" class="img-responsive">
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="count-data-wrapper">
        <div class="count-wrapper">
            <div class="bg-count-primary"></div>
            <div class="container bg-data">
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li class="big-prop">{{$countTotalPopulation}}</li>
                            <li class="small-prop">जनसंख्या</li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li class="big-prop">{{$countTotalOld}}</li>
                            <li class="small-prop">वृद्ध </li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li class="big-prop">{{$countTotalWidow}}</li>
                            <li class="small-prop">एकल महिला</li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li class="big-prop">{{$countTotalPhysicalDisabled}}</li>
                            <li class="small-prop">शारीरिक रूपमा असक्षम </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <script>


        var allLocationDetails = {};
        $('#mapsearch').on('keyup', function(){


            var categoryList = [];
            /*
             $('#categoryLists input').each(function(){
             if($(this).is(':checked')){

             categoryList.push($(this).val());

             }
             })
             */
            if($('.categoryListHome').val() != 'All') {

                categoryList.push($('.categoryListHome').val());
            }

            var csrftoken =  $('meta[name="csrf-token"]').attr('content');
            var value = $(this).val();

            $.ajax({
                url: "getMapSearchResult",
                type: 'POST',
                headers: {'X-CSRF-TOKEN': csrftoken},
                data :{"name" :value,"categorylist":JSON.stringify(categoryList)},
                success: function(result){



                    var location_details = JSON.parse(result);
                    $('#searchfieldlist1').html('');
                    $.each( location_details, function( key  )
                    {
                        allLocationDetails[location_details[key]["kitta_number"]] = location_details[key];

                        $('#searchfieldlist1').append( '<li style="cursor:pointer;margin-top:10px" value="'+location_details[key]["coordinate"]+'" fullname="'+location_details[key]["full_name"]+'" kittanumberhome="'+location_details[key]["kitta_number"]+'" class="kittanumberdataclass">'+location_details[key]['full_name'] +" (" + location_details[key]["kitta_number"]+')</li>')
                    });
                    if(location_details.length >0) { $('#searchfieldlist1').parents('.searchresult').show();} else {$('#searchfieldlist1').parents('.searchresult').hide()}

                    if(location_details.length == 0 && value!='' ){

                        $('#searchfieldlist1').append('<li style=";margin-top:10px">कुनै रेकर्ड फेला परेन</li>')
                        $('#searchfieldlist1').parents('.searchresult').show()
                    }

                }});

        })

    </script>
@endsection