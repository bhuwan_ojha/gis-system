@extends('frontend.layouts.layout')
@section('content')



    <section class="map-wrapper">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-3">
                    <div class="map-sidebar">
                        <h5>नक्सामा हेर्नुहोस्</h5>
                        <div class="dropdown" style="display:none">
                            <button class="btn-outline-white dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Choose Ward
                                <span class="ion-chevron-down"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <div class="form-wrapper">
                            <form action="">
                                <p> फिल्टर दृश्य नियन्त्रण गर्नुहोस्</p>

                                <div class="form-group" id="categoryLists">
                                    <div class="checkbox">
                                        <label><input  type="checkbox" value="all" id="allcategory">सबै</label>
                                    </div>
                                    @foreach($category as $key=>$location)
                                        @if($key =='Private Property' && isset(session()->get('userRoles')->user_roles) && session()->get('userRoles')->user_roles =='manager' )
                                            @continue
                                        @endif

                                            @if($key =='Private Property' && !isset(session()->get('userRoles')->user_roles)  )
                                                @continue
                                            @endif

                                        <div class="checkbox">
                                            <label><input type="checkbox" class="childcheckboxlist" value="{{ $key }}">{{ $location }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="map-view-wrapper">
                        <div class="embed-responsive embed-responsive-100x400px " id="mymap">
                        </div>
                        <div class="map-search">
                            <form action="" class="gis-form-wrapper">
                                <div class="form-group" id="sell2">
                                    <input style="height: 20px" type="text" class="form-control" id="mapsearch" placeholder="यहाँ खोज्नुहोस्">
                                </div>
                            </form>
                        </div>
                        <div class="map-search" style="margin-top:42px;width: 38%;left: 54px">
                            <form action="" class="gis-form-wrapper" style="box-shadow:none !important;border-radius: 4px !important;display: none">
                                <div class="form-group" id="sell2">
                                    <ul  style="list-style-type: none;padding-left: 0px;" id="searchfieldlist">


                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>






    <script>


        var Categortdatalist = JSON.parse( '<?php  echo ($allCategoryLocationJsonData) ?>');


        $(function(){

            $('#mapsearch').on('keyup', function(){

                var categoryList = [];
                $('#categoryLists input').each(function(){
                    if($(this).is(':checked')){

                        categoryList.push($(this).val());

                    }
                })


                var csrftoken =  $('meta[name="csrf-token"]').attr('content');
                var value = $(this).val();

                $.ajax({
                    url: "getMapSearchResult",
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': csrftoken},
                    data :{"name" :value,"categorylist":JSON.stringify(categoryList)},
                    success: function(result){

                        var location_details = JSON.parse(result);
                        $('#searchfieldlist').html('');
                        $.each( location_details, function( key  ) {
                            $('#searchfieldlist').append( '<li style="cursor:pointer;margin-top:10px" value="'+location_details[key]["coordinate"]+'" fullname="'+location_details[key]["full_name"]+'" kittanumber="'+location_details[key]["kitta_number"]+'" class="kittanumberdata">'+location_details[key]['full_name'] +" (" + location_details[key]["kitta_number"]+')</li>')
                        });
                        if(location_details.length >0) { $('#searchfieldlist').parents('form').show();} else {$('#searchfieldlist').parents('form').hide()}

                        if(location_details.length == 0 && value!='' ){

                            $('#searchfieldlist').append('<li style=";margin-top:10px">कुनै रेकर्ड फेला परेन</li>')
                            $('#searchfieldlist').parents('form').show()
                        }

                    }});

            })


            $('#allcategory').on('click',function(){


                if($(this).is(':checked')){
                    $('#categoryLists input').each(function(){
                        $(this).prop('checked', true);
                    })
                } else {
                    $('#categoryLists input').each(function(){
                        $(this).prop('checked', false);
                    })
                }

            })

            $('.childcheckboxlist').on('click',function(){

                if($(this).is(':checked')){

                    if($('.childcheckboxlist:checkbox:checked').length == $('.childcheckboxlist').length) {
                        $('#allcategory').prop('checked', true);
                    }

                } else {

                    $('#allcategory').prop('checked', false);
                }

            })


        })
    </script>

@endsection