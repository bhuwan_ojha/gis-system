<!doctype html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">


    <!-- Ionicons-->
    <link rel="stylesheet" href="/assets/vendor/ionicons/css/ionicons.min.css">


    <!-- fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">


    <link href="/admin-assets/js/summernote/summernote.css" rel="stylesheet">
    <!-- Slick CSS-->
    <link rel="stylesheet" href="/assets/vendor/slick/slick/slick.css">
    <link rel="stylesheet" href="/assets/vendor/slick/slick/slick-theme.css">

    <!-- Custom CSS-->
    <link rel="stylesheet" href="/assets/scss/master.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <script src="/assets/js/jquery.js"></script>

    <!-- bootstrap -->
    <script src="/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- custom javascript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="/assets/js/custom.js"></script>
    <script src="/admin-assets/js/summernote/summernote.js"></script>
    <script src="/assets/mapdata/darchula.geojson"></script>


    <script>


        function initMap() {
            var locationcordinate = {};

            var myLatLng = {lat: 29.811389, lng: 80.872500};



            var map1 = new google.maps.Map(document.getElementById('newmap'), {
                zoom: 10,
                center: myLatLng,
                mapTypeId: 'hybrid',
                labels:true
            });



            map1.data.addGeoJson(darchula);

            map1.data.setStyle({
                fillColor: 'yellow',
                strokeWeight: 1
            });
            var map = new google.maps.Map(document.getElementById('mymap'), {
                zoom: 10,
                center: myLatLng,
                mapTypeId: 'hybrid',
                labels:true
            });

            map.data.addGeoJson(darchula);

            map.data.setStyle({
                fillColor: 'yellow',
                strokeWeight: 1
            });

            var infowindow =  new google.maps.InfoWindow({});



            var CreateMarker = function(kittanumber,coordinates,fullname,mapObject){

                // var kittanumber = kittanumber+fullname.replace(" ", "");
                var kittanumber = kittanumber;
                var cordinateData = coordinates;
                var name = fullname+'  ('+kittanumber+')';
                var namevarialbe = fullname.replace(" ", "");

                var Lat = parseFloat(cordinateData.split(',')[0]);
                var Lng = parseFloat(cordinateData.split(',')[1]);
                //marker.setPosition( new google.maps.LatLng(29.411389, 80.672500 ) );
                mapObject.panTo( new google.maps.LatLng(Lat, Lng ) );

                locationcordinate[kittanumber] = new google.maps.Marker({
                    position: {lat:Lat, lng:Lng},
                    map: mapObject,
                    title: name
                });

                locationcordinate[kittanumber].addListener('click', function() {
                    infowindow.setContent( locationcordinate[kittanumber].getTitle());
                    infowindow.open(mapObject,  locationcordinate[kittanumber]);

                });

            }


            var RemoveMarker = function (){

                $.each(locationcordinate,function(key, value){
                    locationcordinate[key].setMap(null);

                })

            }

            CreateMarker('526','29.811389,80.872500','Gau Palika Office',map);


            $(document).on('click','.kittanumberdata',function(){
                // debugger;
                RemoveMarker()
                $('#mapsearch').val($(this).text());
                $(this).parents('form').hide();

                CreateMarker($(this).attr('kittanumber'),$(this).attr('value'),$(this).attr('fullname'),map);

            })



            $(document).on('change','#categoryLists input:checkbox',function(){
                RemoveMarker()

                $('.childcheckboxlist:checkbox:checked').each(function(){

                    $.each( Categortdatalist[$(this).val()],function(key,value) {

                        CreateMarker(value.kitta_number,value.coordinate,value.full_name,map);


                    })
                })

            })

            $(document).on('click','.kittanumberdataclass',function(){



               var wardDetailsData =  allLocationDetails[$(this).attr('kittanumberhome')];

                CreateMarker($(this).attr('kittanumber'),$(this).attr('value'),$(this).attr('fullname'),map1);

               // debugger;
                $('#modalView .ward-title').text(wardDetailsData.full_name);
                $('#modalView .ward-date').text(wardDetailsData.created_at);

                $('#mapImage').attr('src','/uploads/property/'+wardDetailsData.property_image);

                $('#modalView').modal('show');



            })


        }
    </script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3-6IY2YKaXOCiP0chUXekibYikJfCsyQ&callback=initMap" async defer></script>


    <title>Gis</title>
    <style>
        #dropdownmenu a{
            display: block;
            padding: 5px;
            text-decoration: none;
            color: #1D90C5;
        }

    </style>
</head>
<body>
<nav class="navbar navbar-default custom-navbar navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-4">
                <ul class="list-inline">
                    <li>
                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><img src="/assets/svg/news-tiles.svg" alt="news"></a>
                    </li>
                    <li class="logo">
                        <a href="/"><strong>जीआईएस</strong> </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-xs-8 text-right">
                <ul class="list-inline">
                    <li><a href="/contact"><span><img src="/assets/svg/contact.svg" alt=""></span>सम्पर्क</a></li>
                    <li><a href=""><span><img src="/assets/svg/share.svg" alt=""></span></span>शेयर </a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="collapse collapse-sec" id="collapseExample">
        <div class="card">
            <div class="container-fluid nav">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-body">
                            <h4><i class="fa fa-home"></i> गृह पृष्ठ</h4>
                            <a href="/"></a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card-body">
                            <h4><i class="fa fa-map-marker"></i> नक्शा</h4>
                            <a href="/map"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-body">
                            <h4><i class="fa fa-file-image-o"></i> गैलरी</h4>
                            <a href="/gallery"></a>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-body">
                            <h4><i class="fa fa-newspaper-o"></i> समाचार</h4>
                            <a href="/news"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-body">
                            <h4><i class="fa fa-info"></i> सूचना</h4>
                            <a href="/notice"></a>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="dropdown">

                            <div class="card-body pages-dropdown">
                                <h4 class="pages-dropdown"><i class="fa fa-file"></i> पृष्ठहरू</h4>
                                <a class="pages-dropdown" href="#"></a>

                            </div>
                            <div id="dropdownmenu" style="min-width:50%;display:none;top:90%;position: absolute; background-color: white;border-radius: 4px; padding: 6px" class="" aria-labelledby="dropdownMenuButton">

                                <?php foreach( \App\Libraries\TMHelper::getMenu()  as $slug ) {


                                ?>
                                <a href="<?php echo  '/'.$slug->page_slug; ?>"><?php echo $slug->page_title; ?></a>
                                <?php   }?>


                            </div>

                        </div>
                    </div>






                </div>
            </div>
        </div>
    </div>
</nav>


@yield('content')
        <!-- Modal Search -->
<div id="modalView" class="modal modal-view" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-5">
                        <div class="modal-con-wrapper">
                            <figure>
                                <img id="mapImage" src="/assets/image/ward-img.jpg" alt="">
                            </figure>
                            <ul class="list-unstyled">
                                <li class="small-prop ward-date"><i>Published On May 23</i></li>
                                <li class="big-prop ward-title">Ward title goes here</li>
                            </ul>


                            <a style="display: none" href="" class="btn btn-outline-primary "><span><img src="/assets/svg/print.svg" alt=""></span> Print Document</a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="embed-responsive embed-responsive-100x200px" id="newmap">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="copyright">Copyright © 2019 company name. All rights reserved</p>
    </div>
</footer>


<!-- slick slider-->
<script src="/assets/vendor/slick/slick/slick.min.js"></script>

<!-- jQuery first -->




<script>
    $(document).ready(function() {
        $('.editor').summernote();
    });
</script>
<script>
    $('.image-slider').slick({

        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        cssEase: 'linear'
    });
</script>

<script>
    $('.blog-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
    });
</script>

<!-- lightbox -->
<script>
    $(function(){
        $('#portfolio').magnificPopup({
            delegate: 'a',
            type: 'image',
            image: {
                cursor: null,
                titleSrc: 'title'
            },
            gallery: {
                enabled: true,
                preload: [0,1], // Will preload 0 - before current, and 1 after the current image
                navigateByImgClick: true
            }
        });
    });
</script>

<!-- light box -->
<script>
    window.onload = function() {

        var thumbsList = document.getElementById("thumbsList");
        var allLIs = thumbsList.getElementsByTagName("li");
        var allImages = thumbsList.getElementsByTagName("img");

        var TheLightBox = document.getElementById("lightBox");
        var ThelightBoxImage = document.getElementById("lightBoxImage");

        var prevImg = document.getElementById("prevImg");
        var nextImg = document.getElementById("nextImg");

        var numberOfImages = allLIs.length;
        var counter = 0;
        var currentImage = counter;

        function hasClass(el, className) {
            if (el.classList)
                return el.classList.contains(className)
            else
                return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
        }

        function addClass(el, className) {
            if (el.classList)
                el.classList.add(className)
            else if (!hasClass(el, className)) el.className += " " + className
        }

        function removeClass(el, className) {
            if (el.classList)
                el.classList.remove(className)
            else if (hasClass(el, className)) {
                var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
                el.className=el.className.replace(reg, ' ')
            }
        }

        ThelightBoxImage.setAttribute("src", (allImages[0].getAttribute("src")));

        function loadImageInLightBox(URL, callback){
            var url = URL;
            ThelightBoxImage.setAttribute("src", url);
            if(typeof callback == "function"){
                callback();
            }
        }

        function loadLightBox(){
            addClass(TheLightBox, "lightBox-active");
            addClass(ThelightBoxImage, "active-image");
        }

        function unloadLightBox(){
            removeClass(TheLightBox, "lightBox-active");
            removeClass(ThelightBoxImage, "active-image");
        }

        function prevImage(){
            if(counter <= 0){
                counter = numberOfImages;
            }
            counter--;
            loadImageInLightBox(allImages[counter].getAttribute("src"));
        }

        function nextImage(){
            counter++;
            if(counter == numberOfImages){
                counter = 0;
            }
            loadImageInLightBox(allImages[counter].getAttribute("src"));
        }

        document.getElementById("lightBox").addEventListener("click", function(e){
            switch (e.target.id) {
                case "prevImg":
                    prevImage();
                    break;
                case "nextImg":
                    nextImage();
                    break;
                case "lightBoxImage":
                    break;
                default:
                    unloadLightBox();
            }

        })

        for (var i=0; i < allImages.length; i++){
            allImages[i].setAttribute("id", "thumb_" + i);
            allImages[i].addEventListener("click",
                    function (e){
                        var theIndex =  e.target.id.match(/\d+/)[0];
                        counter = theIndex;
                        loadImageInLightBox(e.target.getAttribute("src"), function(){
                            loadLightBox(e.target.getAttribute("src"));
                        });
                    }
            );
        }

    }


    $(document).ready(function() {
        $(".dropdown").hover(
                function() {
                    $('#dropdownmenu').stop(true, true).slideDown('medium');
                },
                function() {
                    $('#dropdownmenu').stop(true, true).slideUp('medium');
                });
    });

    </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cab6513557d5f68515b829e/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>