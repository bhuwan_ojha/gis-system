@extends('frontend.layouts.layout')
@section('content')
    <section class="notice-wrapper">
    <div class="container">
        <div class="heading-text">
            <h1>समाचार र अद्यावधिकहरू</h1>
        </div>
    </div>
</section>  
<section class="notice-list-wrapper">
<div class="container">
    <div class="news-praper">
        @foreach($newsList as $news)
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-sm-4">
                    <div class="news-image">
                    @if(!empty($news->files))
                        <img src="/uploads/news/{{$news->files}}" alt="notice-file" class="img-responsive">
                    @else
                        <img src="/assets/image/nepal-gov.png" alt="notice-file" class="img-responsive">
                    @endif
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="news-point">
                        <h4>{{$news->news_title}}</h4>
                        <p>{{substr(strip_tags($news->news_description),0,102)}}...</p>
                        <ul class="list-unstyled">
                            <li>प्रकाशित मिति <i> {{' '.date_format($news->created_at,"l jS F Y")}}</i><span class="pull-right"><a href="/news/{{$news->news_of_ward_no}}/{{$news->id}}/news-detail">थप पढ्नुहोस्</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
         @endforeach
    </div>
    <div class="pagination-wrapper text-center">{{$newsList->links()}}</div>
</div>
</section>
@endsection